# samurai-2019

samurai-2019 のリポジトリ

## links

* [環境構築](https://gitlab.com/hinhi/samurai-2019/wikis/環境構築
)
* [ゲームルール](http://samuraicoding.info/rules-jp.html)
* [software-for-SamurAI-Coding-2018-19](https://github.com/takashi-chikayama/software-for-SamurAI-Coding-2018-19)
* [wiki](https://gitlab.com/hinhi/samurai-2019/wikis/SamurAI-Coding-2019-%E3%81%84%E3%81%84%E7%94%9F%E6%B4%BB%E3%83%81%E3%83%BC%E3%83%A0-wiki)
* [issue](https://gitlab.com/hinhi/samurai-2019/issues)
