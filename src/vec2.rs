use std::borrow::Cow;
use std::cmp::{max, min};
use std::collections::HashMap;
use std::iter::Iterator;
use std::ops::{Add, Mul, Sub};

/// 2要素ベクトル
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
pub struct Vec2 {
    pub x: isize,
    pub y: isize,
}

pub const NINE_DIR: [Vec2; 9] = [
    Vec2 { x: -1, y: -1 },
    Vec2 { x: -1, y: 0 },
    Vec2 { x: -1, y: 1 },
    Vec2 { x: 0, y: -1 },
    Vec2 { x: 0, y: 0 },
    Vec2 { x: 0, y: 1 },
    Vec2 { x: 1, y: -1 },
    Vec2 { x: 1, y: 0 },
    Vec2 { x: 1, y: 1 },
];

impl Vec2 {
    pub fn new(x: isize, y: isize) -> Vec2 {
        Vec2 { x, y }
    }

    fn touched_pos(&self) -> Vec<Vec2> {
        if self.x == 0 {
            if self.y >= 0 {
                (1..=self.y).map(|y| Vec2::new(0, y)).collect()
            } else {
                (1..=-self.y).map(|y| Vec2::new(0, -y)).collect()
            }
        } else if self.y == 0 {
            if self.x >= 0 {
                (1..=self.x).map(|x| Vec2::new(x, 0)).collect()
            } else {
                (1..=-self.x).map(|x| Vec2::new(-x, 0)).collect()
            }
        } else {
            let (dx, sx) = if self.x >= 0 {
                (self.x, 1)
            } else {
                (-self.x, -1)
            };
            let (dy, sy) = if self.y >= 0 {
                (self.y, 1)
            } else {
                (-self.y, -1)
            };
            let mut r = Vec::new();

            for y in (1..).take_while(|y| dx * (2 * y - 1) <= dy) {
                r.push(Vec2::new(0, sy * y));
            }

            for x in (0..).map(|x| 2 * x + 1).take_while(|&x| x < 2 * dx - 1) {
                let mut y_start = (dy * x + dx) / (2 * dx);
                if dy * x + dx == (dy * x + dx) / (2 * dx) * (2 * dx) {
                    y_start -= 1;
                }
                for y in (y_start..).take_while(|y| dx * (2 * y - 1) <= dy * (x + 2)) {
                    r.push(Vec2::new(sx * (x + 1) / 2, sy * y));
                }
            }

            let mut y_start = (dy * (2 * dx - 1) + dx) / (2 * dx);
            if (dy * (2 * dx - 1) + dx) == (dy * (2 * dx - 1) + dx) / (2 * dx) * (2 * dx) {
                y_start -= 1;
            }
            for y in y_start..=dy {
                r.push(Vec2::new(sx * dx, sy * y));
            }
            r
        }
    }
}

impl Add for Vec2 {
    type Output = Vec2;
    fn add(self, rhs: Self) -> Self::Output {
        Vec2 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl Add for &Vec2 {
    type Output = Vec2;
    fn add(self, rhs: Self) -> Self::Output {
        Vec2 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl Sub for Vec2 {
    type Output = Vec2;
    fn sub(self, rhs: Self) -> Self::Output {
        Vec2 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl Mul<isize> for Vec2 {
    type Output = Vec2;
    fn mul(self, rhs: isize) -> Self::Output {
        Vec2 {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}

/// 「移動」
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Movement {
    pub from: Vec2,
    pub to: Vec2,
}

impl Movement {
    pub fn new(from: Vec2, to: Vec2) -> Movement {
        Movement { from, to }
    }

    /// 別の移動との交錯判定
    ///
    /// 公式のコードから移植
    pub fn intersects(&self, other: &Movement) -> bool {
        let min_sx = min(self.from.x, self.to.x);
        let max_sx = max(self.from.x, self.to.x);
        let min_ox = min(other.from.x, other.to.x);
        let max_ox = max(other.from.x, other.to.x);
        if max_sx < min_ox || max_ox < min_sx {
            return false;
        }
        let min_sy = min(self.from.y, self.to.y);
        let max_sy = max(self.from.y, self.to.y);
        let min_oy = min(other.from.y, other.to.y);
        let max_oy = max(other.from.y, other.to.y);
        if max_sy < min_oy || max_oy < min_sy {
            return false;
        }
        let d1 = (self.from.x - other.from.x) * (other.to.y - other.from.y)
            - (self.from.y - other.from.y) * (other.to.x - other.from.x);
        let d2 = (self.to.x - other.from.x) * (other.to.y - other.from.y)
            - (self.to.y - other.from.y) * (other.to.x - other.from.x);
        if d1 * d2 > 0 {
            return false;
        }
        let d3 = (other.from.x - self.from.x) * (self.to.y - self.from.y)
            - (other.from.y - self.from.y) * (self.to.x - self.from.x);
        let d4 = (other.to.x - self.from.x) * (self.to.y - self.from.y)
            - (other.to.y - self.from.y) * (self.to.x - self.from.x);
        if d3 * d4 > 0 {
            return false;
        }
        true
    }

    /// 移動するなかで触れるマス目の一覧を計算する
    ///
    /// 公式から大まかには移植。
    ///
    /// スタート地点は障害ではないはずでそこの判定は不要なはず。
    /// なのでスタート地点を省略したマス目一覧を返している。
    ///
    /// イテレータにしたいところだが、意外としんどいのでVecで返している。
    pub fn touched_pos(&self) -> TouchedPos {
        let key = self.to - self.from;
        match TOUCHED_POS.get(&key) {
            Some(v) => TouchedPos {
                n: 0,
                v: Cow::from(v),
                o: self.from,
            },
            None => TouchedPos {
                n: 0,
                v: Cow::from(key.touched_pos()),
                o: self.from,
            },
        }
    }
}

lazy_static! {
    static ref TOUCHED_POS: HashMap<Vec2, Vec<Vec2>> = {
        let mut m = HashMap::new();
        for x in -10..=10 {
            for y in -10..=10 {
                let key = Vec2::new(x, y);
                let value = key.touched_pos();
                m.insert(key, value);
            }
        }
        m
    };
}

#[derive(Debug)]
pub struct TouchedPos {
    n: usize,
    v: Cow<'static, [Vec2]>,
    o: Vec2,
}

impl Iterator for TouchedPos {
    type Item = Vec2;
    fn next(&mut self) -> Option<Self::Item> {
        if self.n == self.v.len() {
            return None;
        }
        self.n += 1;
        Some(self.v[self.n - 1] + self.o)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn vec2_it_work() {
        assert_eq!(Vec2::new(1, 2), Vec2::new(1, 2));
        assert_ne!(Vec2::new(1, 2), Vec2::new(1, 3));
        assert_ne!(Vec2::new(2, 2), Vec2::new(1, 2));
    }

    #[test]
    fn vec2_add() {
        let a = Vec2::new(1, 2);
        let b = Vec2::new(3, 4);
        let c = a + b;
        assert_eq!(a.x, 1);
        assert_eq!(c.x, 4);
        assert_eq!(c.y, 6);
    }

    #[test]
    fn vec2_sub() {
        let a = Vec2::new(1, 2);
        let b = Vec2::new(3, 4);
        let c = a - b;
        assert_eq!(a.x, 1);
        assert_eq!(c.x, -2);
        assert_eq!(c.y, -2);
    }

    #[test]
    fn vec2_mul() {
        let a = Vec2::new(1, 2);
        let c = a * 2;
        assert_eq!(a.x, 1);
        assert_eq!(c.x, 2);
        assert_eq!(c.y, 4);
    }

    #[test]
    fn move_intersects() {
        let test_cases = vec![
            ([0, 0, 0, 0], [0, 0, 0, 0], true),
            ([1, 1, 1, 1], [0, 0, 0, 0], false),
            ([1, 1, 1, 1], [0, 0, 2, 2], true),
            ([0, 0, 1, 1], [1, 0, 0, 1], true),
            ([1, 0, 3, 3], [2, 2, 2, 2], false),
        ];
        for case in test_cases {
            let a = case.0;
            let ma = Movement::new(Vec2::new(a[0], a[1]), Vec2::new(a[2], a[3]));
            let b = case.1;
            let mb = Movement::new(Vec2::new(b[0], b[1]), Vec2::new(b[2], b[3]));
            assert_eq!(ma.intersects(&mb), case.2);
        }
    }

    #[test]
    fn touched_pos() {
        // y方向
        let m = Movement::new(Vec2::new(1, 1), Vec2::new(1, 3));
        assert_eq!(
            m.touched_pos().collect::<Vec<_>>(),
            vec![Vec2::new(1, 2), Vec2::new(1, 3)]
        );
        let m = Movement::new(Vec2::new(1, 3), Vec2::new(1, 1));
        assert_eq!(
            m.touched_pos().collect::<Vec<_>>(),
            vec![Vec2::new(1, 2), Vec2::new(1, 1)]
        );
        // x方向
        let m = Movement::new(Vec2::new(1, 1), Vec2::new(3, 1));
        assert_eq!(
            m.touched_pos().collect::<Vec<_>>(),
            vec![Vec2::new(2, 1), Vec2::new(3, 1)]
        );
        let m = Movement::new(Vec2::new(3, 1), Vec2::new(1, 1));
        assert_eq!(
            m.touched_pos().collect::<Vec<_>>(),
            vec![Vec2::new(2, 1), Vec2::new(1, 1)]
        );
        // 斜め
        let m = Movement::new(Vec2::new(1, 1), Vec2::new(2, 5));
        assert_eq!(
            m.touched_pos().collect::<Vec<_>>(),
            vec![
                Vec2::new(1, 2),
                Vec2::new(1, 3),
                Vec2::new(2, 3),
                Vec2::new(2, 4),
                Vec2::new(2, 5),
            ]
        );
        let m = Movement::new(Vec2::new(1, 1), Vec2::new(2, 4));
        assert_eq!(
            m.touched_pos().collect::<Vec<_>>(),
            vec![
                Vec2::new(1, 2),
                Vec2::new(1, 3),
                Vec2::new(2, 2),
                Vec2::new(2, 3),
                Vec2::new(2, 4),
            ]
        );
        let m = Movement::new(Vec2::new(1, 1), Vec2::new(5, 2));
        assert_eq!(
            m.touched_pos().collect::<Vec<_>>(),
            vec![
                Vec2::new(2, 1),
                Vec2::new(3, 1),
                Vec2::new(3, 2),
                Vec2::new(4, 2),
                Vec2::new(5, 2),
            ]
        );
        let m = Movement::new(Vec2::new(2, 5), Vec2::new(1, 1));
        assert_eq!(
            m.touched_pos().collect::<Vec<_>>(),
            vec![
                Vec2::new(2, 4),
                Vec2::new(2, 3),
                Vec2::new(1, 3),
                Vec2::new(1, 2),
                Vec2::new(1, 1),
            ]
        );
        let m = Movement::new(Vec2::new(5, 2), Vec2::new(1, 1));
        assert_eq!(
            m.touched_pos().collect::<Vec<_>>(),
            vec![
                Vec2::new(4, 2),
                Vec2::new(3, 2),
                Vec2::new(3, 1),
                Vec2::new(2, 1),
                Vec2::new(1, 1),
            ]
        );
        let m = Movement::new(Vec2::new(2, 0), Vec2::new(1, 4));
        assert_eq!(
            m.touched_pos().collect::<Vec<_>>(),
            vec![
                Vec2::new(2, 1),
                Vec2::new(2, 2),
                Vec2::new(1, 2),
                Vec2::new(1, 3),
                Vec2::new(1, 4),
            ]
        );
    }
}
