use std::io::{self, BufRead};
use std::iter::Iterator;

use crate::vec2::{Movement, Vec2};

/// プレイヤー=AIの位置と速度を保持する
///
/// ゲームの中でのかなり軽量なデータコンテナに過ぎない。
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct Player {
    length: isize,
    pub position: Vec2,
    pub velocity: Vec2,
}

impl Player {
    pub fn new(length: usize, position: Vec2, velocity: Vec2) -> Player {
        Player {
            length: length as isize,
            position,
            velocity,
        }
    }

    /// ゲーム管理システムからの入力からインスタンスを生成
    ///
    /// 不正な入力をチェックしていない。
    pub fn from_reader<R: BufRead>(length: usize, r: &mut R) -> io::Result<Player> {
        let mut line = String::new();
        r.read_line(&mut line)?;
        let mut nums = line.split_whitespace();
        let x = nums.next().unwrap().parse().unwrap();
        let y = nums.next().unwrap().parse().unwrap();
        let vx = nums.next().unwrap().parse().unwrap();
        let vy = nums.next().unwrap().parse().unwrap();
        Ok(Player::new(length, vec2!(x, y), vec2!(vx, vy)))
    }

    /// 自分がレース中、つまり自分がまだゴールしていなかったら `true` を返す
    pub fn is_racing(&self) -> bool {
        self.position.y < self.length
    }

    /// 次の1ターンでの「移動」を計算して返す
    ///
    /// 基本的に衝突判定に用いる。
    pub fn get_movement(&self, acceleration: Vec2) -> Movement {
        Movement::new(self.position, self.position + self.velocity + acceleration)
    }

    /// 最後の1ターンでゴールを行き過ぎたときのゴールタイムをちゃんと計算するための関数
    ///
    /// 未ゴールだと`None`、ゴールしていると1以下の`Some(f64)`を返す。
    pub fn goal_time(&self) -> Option<f64> {
        if self.is_racing() {
            return None;
        };
        let y = self.position.y - self.velocity.y;
        let l = self.length as isize - y;
        Some(l as f64 / self.velocity.y as f64)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::BufReader;

    #[test]
    fn from_reader() {
        let mut input = BufReader::new("4 6 -1 3\n0 100 0 0\n".as_bytes());
        let p = Player::from_reader(100, &mut input).unwrap();
        assert_eq!(p.position, Vec2::new(4, 6));
        assert_eq!(p.velocity, Vec2::new(-1, 3));
        assert!(p.is_racing());
        let p = Player::from_reader(100, &mut input).unwrap();
        assert_eq!(p.position, Vec2::new(0, 100));
        assert_eq!(p.velocity, Vec2::new(0, 0));
        assert!(!p.is_racing());
    }

    #[test]
    fn goal_time() {
        assert_eq!(Player::new(10, vec2!(0, 9), vec2!(0, 1)).goal_time(), None);
        assert_eq!(
            Player::new(10, vec2!(0, 10), vec2!(0, 1)).goal_time(),
            Some(1.0)
        );
        assert_eq!(
            Player::new(10, vec2!(0, 11), vec2!(0, 2)).goal_time(),
            Some(0.5)
        );
    }
}
