#[macro_use]
extern crate lazy_static;

#[macro_use]
pub mod macros;
mod ai;
mod course;
mod player;
mod race;
mod searcher;
mod vec2;

pub use crate::ai::*;
pub use crate::course::*;
pub use crate::player::Player;
pub use crate::race::*;
pub use crate::searcher::*;
pub use crate::vec2::*;
