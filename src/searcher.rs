/// 探索器
use std::cmp::{Ord, Ordering, PartialEq, PartialOrd};
use std::collections::{BinaryHeap, HashSet};
use std::f64;
use std::time::{Duration, Instant};

use rand::{seq::SliceRandom, Rng};

use crate::course::{CostGrad, CostMap};
use crate::race::SimulatedRaceStep;
use crate::vec2::{Vec2, NINE_DIR};

/// 探索器のインターフェース
pub trait Searcher {
    fn search<R: Rng, M: CostMap, G: CostGrad>(
        &self,
        random: &mut R,
        cost_map: &M,
        cost_grad: &G,
        start_step: &SimulatedRaceStep,
    ) -> (Vec2, f64);
}

/// 3x3=9通りの加速度の配列をランダムな順番にして返す便利関数
fn get_random_accelerations<R: Rng>(rng: &mut R) -> [Vec2; 9] {
    // それにしても配列を返したいがために（=Vecを使わないために）
    // こんなにもひどいコードを書くとは。
    let mut pos = [0, 1, 2, 3, 4, 5, 6, 7, 8];
    pos.shuffle(rng);
    [
        NINE_DIR[pos[0]],
        NINE_DIR[pos[1]],
        NINE_DIR[pos[2]],
        NINE_DIR[pos[3]],
        NINE_DIR[pos[4]],
        NINE_DIR[pos[5]],
        NINE_DIR[pos[6]],
        NINE_DIR[pos[7]],
        NINE_DIR[pos[8]],
    ]
}

/// ビームサーチ等で使う汎用的な評価関数
fn calc_cost<C: CostMap>(
    cost_map: &C,
    current_step: &SimulatedRaceStep,
    next_step: &SimulatedRaceStep,
) -> f64 {
    if let Some(cost) = next_step.players[0].goal_time() {
        current_step.current_step as f64 + cost
    } else {
        next_step.current_step as f64 + cost_map.get_cost(&next_step.players[0].position)
    }
}

/// ビームサーチ用の途中状態
struct SearchState<'a> {
    first_turn_id: usize, // 1手目のインデックス
    step: SimulatedRaceStep<'a>,
    cost: f64,
}

impl<'a> PartialEq for SearchState<'a> {
    fn eq(&self, other: &SearchState) -> bool {
        self.cost == other.cost
    }
}

impl<'a> Eq for SearchState<'a> {}

impl<'a> PartialOrd for SearchState<'a> {
    fn partial_cmp(&self, other: &SearchState) -> Option<Ordering> {
        self.cost.partial_cmp(&other.cost)
    }
}

impl<'a> Ord for SearchState<'a> {
    fn cmp(&self, other: &SearchState) -> Ordering {
        self.partial_cmp(&other).unwrap()
    }
}

/// Chokudaiサーチ
pub struct ChokudaiSearcher {
    max_depth: usize,
    beam_width: usize,
    duration: Duration,
}

impl ChokudaiSearcher {
    pub fn new(max_depth: usize, beam_width: usize, duration: Duration) -> ChokudaiSearcher {
        ChokudaiSearcher {
            max_depth,
            beam_width,
            duration,
        }
    }
}

impl Searcher for ChokudaiSearcher {
    fn search<R: Rng, M: CostMap, G: CostGrad>(
        &self,
        random: &mut R,
        cost_map: &M,
        cost_grad: &G,
        start_step: &SimulatedRaceStep,
    ) -> (Vec2, f64) {
        // 探索開始時間
        let now = Instant::now();
        // 状態たち
        let mut states = Vec::with_capacity(self.max_depth);
        let mut mem = Vec::with_capacity(self.max_depth);
        for _ in 0..self.max_depth {
            states.push(BinaryHeap::new());
            mem.push(HashSet::new());
        }
        // 1手目を特別扱いしている
        let first_step_accelerations = get_random_accelerations(random);
        for (i, &a) in first_step_accelerations.iter().enumerate() {
            let b = cost_grad.get_grad(&start_step.players[1].position);
            let next_step = start_step.simulate([a, b]);
            if mem[0].insert(next_step.play_key()) {
                let cost = calc_cost(cost_map, &start_step, &next_step);
                states[0].push(SearchState {
                    first_turn_id: i,
                    step: next_step,
                    cost: -cost,
                });
            }
        }
        let mut best = (vec2!(0, 1), f64::INFINITY);
        while now.elapsed() < self.duration {
            for depth in 0..self.max_depth {
                for _ in 0..self.beam_width {
                    let state = match states[depth].pop() {
                        Some(s) => s,
                        None => break,
                    };
                    // ゴールしてたらそこで終わり
                    if !state.step.self_racing() {
                        if best.1 > -state.cost {
                            best = (first_step_accelerations[state.first_turn_id], -state.cost);
                        }
                        continue;
                    }
                    // 次の状態の列挙
                    for a in NINE_DIR.iter() {
                        let b = cost_grad.get_grad(&state.step.players[1].position);
                        let next_step = state.step.simulate([*a, b]);
                        // オリジナルだと最終状態もheapに貯めるが、如何にも無駄なのでdepthで分岐する
                        if depth + 1 < self.max_depth {
                            if mem[depth + 1].insert(next_step.play_key()) {
                                let cost = calc_cost(cost_map, &state.step, &next_step);
                                states[depth + 1].push(SearchState {
                                    first_turn_id: state.first_turn_id,
                                    step: next_step,
                                    cost: -cost,
                                });
                            }
                        } else {
                            let cost = calc_cost(cost_map, &state.step, &next_step);
                            if best.1 > cost {
                                best = (first_step_accelerations[state.first_turn_id], cost);
                            }
                        }
                    }
                }
            }
        }
        best
    }
}

/// ビームサーチによる探索
#[derive(Debug)]
pub struct BeamSearcher {
    max_depth: usize,
    beam_width: usize,
    duration: Duration,
}

impl BeamSearcher {
    pub fn new(max_depth: usize, beam_width: usize, duration: Duration) -> BeamSearcher {
        BeamSearcher {
            max_depth,
            beam_width,
            duration,
        }
    }

    /// 全ての要素が同じところから派生しているなら`true`を返す
    /// 全ての要素が同じ行動から派生しているならそれ以上の探索は不要なので
    fn is_only_one(&self, states: &BinaryHeap<SearchState>) -> bool {
        let id = states.peek().unwrap().first_turn_id;
        states.iter().all(|s| s.first_turn_id == id)
    }
}

impl Searcher for BeamSearcher {
    fn search<R: Rng, M: CostMap, G: CostGrad>(
        &self,
        random: &mut R,
        cost_map: &M,
        cost_grad: &G,
        start_step: &SimulatedRaceStep,
    ) -> (Vec2, f64) {
        // 探索開始時間
        let now = Instant::now();
        // 1手目を特別扱いしている
        let first_step_accelerations = get_random_accelerations(random);
        let mut states = BinaryHeap::new();
        let mut is_goal = false;
        // 1手目の `mem` のスコープが気持ち悪いのでスコープを明示的に切っている
        // 実際上は必要ないスコープ
        {
            let mut mem = HashSet::new();
            for (i, &a) in first_step_accelerations.iter().enumerate() {
                let b = cost_grad.get_grad(&start_step.players[1].position);
                let next_step = start_step.simulate([a, b]);
                if mem.insert(next_step.play_key()) {
                    let cost = calc_cost(cost_map, &start_step, &next_step);
                    is_goal = is_goal || !next_step.self_racing();
                    states.push(SearchState {
                        first_turn_id: i,
                        step: next_step,
                        cost,
                    });
                    if states.len() > self.beam_width {
                        states.pop();
                    }
                }
            }
        }
        for _ in 1..self.max_depth {
            if now.elapsed() >= self.duration {
                break;
            }
            // この段階でゴールできたならそれ以上の行動は見つからないはずなのでこの段階でベストな行動を選択
            if is_goal {
                let s = states.iter().min().unwrap();
                return (first_step_accelerations[s.first_turn_id], s.cost);
            }
            // 今後の探索必要性をチェック
            if self.is_only_one(&states) {
                let s = states.peek().unwrap();
                return (first_step_accelerations[s.first_turn_id], s.cost);
            }
            // 次のステップの状態を保持するコレクション
            let mut next_states = BinaryHeap::with_capacity(self.beam_width + 1);
            let mut mem = HashSet::with_capacity(states.len() * 8);
            // 次のステップを生成
            for state in states {
                for a in NINE_DIR.iter() {
                    let b = cost_grad.get_grad(&state.step.players[1].position);
                    let next_step = state.step.simulate([*a, b]);
                    if mem.insert(next_step.play_key()) {
                        let cost = calc_cost(cost_map, &state.step, &next_step);
                        is_goal = is_goal || !next_step.self_racing();
                        next_states.push(SearchState {
                            first_turn_id: state.first_turn_id,
                            step: next_step,
                            cost,
                        });
                        if next_states.len() > self.beam_width {
                            next_states.pop();
                        }
                    }
                }
            }
            states = next_states;
        }
        // コスト最小を選択
        let s = states.iter().min().unwrap();
        (first_step_accelerations[s.first_turn_id], s.cost)
    }
}
