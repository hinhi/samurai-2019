use std::io::{stdin, stdout, Write};

use rand::SeedableRng;
use rand_xorshift::XorShiftRng;

use samurai_2019::*;

fn main() {
    let stdin = stdin();
    let mut cin = stdin.lock();
    let stdout = stdout();
    let mut cout = stdout.lock();
    let mut random = XorShiftRng::seed_from_u64(1);

    let depth_05 = 15;
    let depth_10 = 12;
    let depth_15 = 10;
    let depth_20 = 10;
    let width_05 = 5;
    let width_10 = 30;
    let width_15 = 60;
    let width_20 = 120;
    let multi_05 = 6;
    let multi_10 = 4;
    let multi_15 = 4;
    let multi_20 = 4;

    let race = Race::from_reader(&mut cin).unwrap();
    let (max_depth, beam_width, multiplier) = if race.width <= 10 {
        let t = race.width - 5;
        (
            interpolation(depth_05, depth_10, t),
            interpolation(width_05, width_10, t),
            interpolation(multi_05, multi_10, t),
        )
    } else if race.width <= 15 {
        let t = race.width - 10;
        (
            interpolation(depth_10, depth_15, t),
            interpolation(width_10, width_15, t),
            interpolation(multi_10, multi_15, t),
        )
    } else if race.width <= 20 {
        let t = race.width - 15;
        (
            interpolation(depth_15, depth_20, t),
            interpolation(width_15, width_20, t),
            interpolation(multi_15, multi_20, t),
        )
    } else {
        (depth_20, width_20, multi_20)
    };

    let searcher_factory = BeamSearcherFactory::new(max_depth, beam_width);
    let cost_factory = CleverCostFactory::new();

    let course_generator = KiaiGenerator::new(race.width);
    let ai = MultiThreadAI::new(
        multiplier,
        race,
        searcher_factory,
        cost_factory,
        course_generator,
    );
    writeln!(cout, "0").unwrap();
    cout.flush().unwrap();
    while let Some(acc) = ai.planing(&mut cin, &mut random) {
        writeln!(cout, "{} {}", acc.x, acc.y).unwrap();
        cout.flush().unwrap();
    }
}
