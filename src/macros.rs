#[macro_export]
macro_rules! vec2 {
    ($x:expr, $y:expr) => {
        Vec2::new($x, $y)
    };
}

#[macro_export]
macro_rules! single_ai_main {
    ($SearcherFactory:ident, $CostFactory:ident) => {
        use std::env::args;
        use std::io::{stdin, stdout, Write};

        use rand::SeedableRng;
        use rand_xorshift::XorShiftRng;

        fn main() {
            let stdin = stdin();
            let mut cin = stdin.lock();
            let stdout = stdout();
            let mut cout = stdout.lock();
            let mut random = XorShiftRng::seed_from_u64(1);

            let args = args().skip(1).collect::<Vec<_>>();
            let max_depth = args[0].trim().parse().unwrap();
            let beam_width = args[1].trim().parse().unwrap();

            let searcher_factory = $SearcherFactory::new(max_depth, beam_width);
            let cost_factory = $CostFactory::new();

            let race = Race::from_reader(&mut cin).unwrap();
            let course_generator = KiaiGenerator::new(race.width);
            let ai = SingleThreadAI::new(race, searcher_factory, cost_factory, course_generator);
            writeln!(cout, "0").unwrap();
            cout.flush().unwrap();
            while let Some(acc) = ai.planing(&mut cin, &mut random) {
                writeln!(cout, "{} {}", acc.x, acc.y).unwrap();
                cout.flush().unwrap();
            }
        }
    };
}

#[macro_export]
macro_rules! multi_ai_main {
    ($SearcherFactory:ident, $CostFactory:ident) => {
        use std::env::args;
        use std::io::{stdin, stdout, Write};

        use rand::SeedableRng;
        use rand_xorshift::XorShiftRng;

        fn main() {
            let stdin = stdin();
            let mut cin = stdin.lock();
            let stdout = stdout();
            let mut cout = stdout.lock();
            let mut random = XorShiftRng::seed_from_u64(1);

            let args = args().skip(1).collect::<Vec<_>>();
            let max_depth = args[0].trim().parse().unwrap();
            let beam_width = args[1].trim().parse().unwrap();
            let multiplier = args[2].trim().parse().unwrap();

            let searcher_factory = $SearcherFactory::new(max_depth, beam_width);
            let cost_factory = $CostFactory::new();

            let race = Race::from_reader(&mut cin).unwrap();
            let course_generator = KiaiGenerator::new(race.width);
            let ai = MultiThreadAI::new(
                multiplier,
                race,
                searcher_factory,
                cost_factory,
                course_generator,
            );
            writeln!(cout, "0").unwrap();
            cout.flush().unwrap();
            while let Some(acc) = ai.planing(&mut cin, &mut random) {
                writeln!(cout, "{} {}", acc.x, acc.y).unwrap();
                cout.flush().unwrap();
            }
        }
    };
}

#[macro_export]
macro_rules! multi_grad_ai_main {
    ($SearcherFactory:ident, $CostFactory:ident) => {
        use std::env::args;
        use std::io::{stdin, stdout, Write};

        use rand::SeedableRng;
        use rand_xorshift::XorShiftRng;

        fn main() {
            let stdin = stdin();
            let mut cin = stdin.lock();
            let stdout = stdout();
            let mut cout = stdout.lock();
            let mut random = XorShiftRng::seed_from_u64(1);

            let args = args().skip(1).collect::<Vec<_>>();
            let max_depth = args[0].trim().parse().unwrap();
            let beam_width = args[1].trim().parse().unwrap();
            let multiplier = args[2].trim().parse().unwrap();
            let y = args[3].trim().parse().unwrap();

            let searcher_factory = $SearcherFactory::new(max_depth, beam_width);
            let map_factory = $CostFactory::new();
            let grad_factory = ConstGradFactory::new(y);

            let race = Race::from_reader(&mut cin).unwrap();
            let course_generator = KiaiGenerator::new(race.width);
            let ai = MultiThreadGradAI::new(
                multiplier,
                race,
                searcher_factory,
                map_factory,
                grad_factory,
                course_generator,
            );
            writeln!(cout, "0").unwrap();
            cout.flush().unwrap();
            while let Some(acc) = ai.planing(&mut cin, &mut random) {
                writeln!(cout, "{} {}", acc.x, acc.y).unwrap();
                cout.flush().unwrap();
            }
        }
    };
}

#[cfg(test)]
mod tests {
    use crate::vec2::Vec2;

    #[test]
    fn vec2_macro() {
        assert_eq!(vec2!(1, 2), Vec2::new(1, 2));
    }
}
