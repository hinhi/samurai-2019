/// コストマップ
use std::cmp::{Eq, Ord, Ordering, PartialEq, PartialOrd};
use std::collections::BinaryHeap;
use std::f64;
use std::fmt::{self, Display, Write};

use super::{Course, JimenType};
use crate::vec2::{Movement, Vec2};

/// プレイヤーの位置からゴールまでに必要な時間を近似するインターフェース
///
/// ステップ数と同じスケールである必要がある。
pub trait CostMap {
    fn get_cost(&self, position: &Vec2) -> f64;
}

/// プレイヤーの位置から次に移動しそうな加速度を近似するインターフェース
///
/// 加速度の各成分の大きさは1以下である必要がある
pub trait CostGrad {
    fn get_grad(&self, position: &Vec2) -> Vec2;
}

/// ゴールからのマンハッタン距離を必要ステップ数として扱うナイーブモデル
pub struct NaiveCost {
    width: isize,
    length: isize,
    costs: Vec<Vec<f64>>,
}

/// ゴールからの距離を計算するためのダイクストラするための状態
#[derive(Copy, Clone, Debug)]
struct CostState {
    cost: f64,
    pos: Vec2,
}

impl PartialEq for CostState {
    fn eq(&self, other: &Self) -> bool {
        self.cost.eq(&other.cost)
    }
}

impl Eq for CostState {}

impl PartialOrd for CostState {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        // 最小のものをheap先頭にするために大小比較を逆にするc
        other.cost.partial_cmp(&self.cost)
    }
}

impl Ord for CostState {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl NaiveCost {
    pub fn new(course: &Course) -> NaiveCost {
        let mut costs = Vec::with_capacity(course.length);
        for _ in 0..course.length {
            costs.push(vec![f64::INFINITY; course.width]);
        }
        let mut heap = BinaryHeap::new();
        for x in 0..course.width {
            if course.get(x, course.length - 1) != JimenType::Shogai {
                let default_cost = 1.0;
                costs[course.length - 1][x] = default_cost;
                heap.push(CostState {
                    cost: default_cost,
                    pos: Vec2::new(x as isize, (course.length - 1) as isize),
                });
            }
        }
        while let Some(state) = heap.pop() {
            for nei in course.neighbors(state.pos) {
                let next_cost = state.cost + 1.0;
                if next_cost < costs[nei.y as usize][nei.x as usize] {
                    costs[nei.y as usize][nei.x as usize] = next_cost;
                    heap.push(CostState {
                        cost: next_cost,
                        pos: nei,
                    })
                }
            }
        }
        NaiveCost {
            width: course.width as isize,
            length: course.length as isize,
            costs,
        }
    }
}

impl CostMap for NaiveCost {
    fn get_cost(&self, position: &Vec2) -> f64 {
        if position.x < 0 || self.width <= position.x {
            return f64::INFINITY;
        }
        if self.length <= position.y {
            return 0.0;
        }
        // これ微妙
        if position.y < 0 {
            let m = self.costs[0]
                .iter()
                .enumerate()
                .map(|(x, c)| {
                    let dx = (x as isize - position.x).abs();
                    dx as f64 + c
                })
                .fold(f64::NAN, f64::min);
            return m - position.y as f64;
        }
        self.costs[position.y as usize][position.x as usize]
    }
}

impl CostGrad for NaiveCost {
    fn get_grad(&self, position: &Vec2) -> Vec2 {
        let x0 = self.get_cost(&(position + &vec2!(-1, 0)));
        let x1 = self.get_cost(&(position + &vec2!(1, 0)));
        let y0 = self.get_cost(&(position + &vec2!(0, -1)));
        let y1 = self.get_cost(&(position + &vec2!(0, 1)));
        let x = if x0 > x1 {
            1
        } else if x0 < x1 {
            -1
        } else {
            0
        };
        let y = if y0 > y1 {
            1
        } else if y0 < y1 {
            -1
        } else {
            0
        };
        vec2!(x, y)
    }
}

impl Display for NaiveCost {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let th = (self.width * self.length * 10) as f64;
        for row in self.costs.iter().rev() {
            let mut buf = String::new();
            for c in row {
                if *c > th {
                    write!(buf, " *").unwrap();
                } else {
                    write!(buf, " {}", c).unwrap();
                }
            }
            buf.push('\n');
            f.write_str(&buf)?;
        }
        Ok(())
    }
}

/// あまりクレバーじゃないコストモデル
///
/// 斜めの扱いが改良されてる
pub struct CleverCost {
    width: isize,
    length: isize,
    costs: Vec<Vec<f64>>,
}

impl CleverCost {
    pub fn new(course: &Course) -> CleverCost {
        let mut costs = vec![vec![f64::INFINITY; course.width]; course.length];

        let mut heap = BinaryHeap::new();
        for x in 0..course.width {
            if course.get(x, course.length - 1) != JimenType::Shogai {
                let default_cost = 1.0;
                costs[course.length - 1][x] = default_cost;
                heap.push(CostState {
                    cost: default_cost,
                    pos: Vec2::new(x as isize, (course.length - 1) as isize),
                });
            }
        }

        fn search(course: &Course, costs: &Vec<Vec<f64>>, now: Vec2) -> f64 {
            let mut best = f64::INFINITY;
            for y in -1..=1 {
                for x in -1..=1 {
                    let m = Movement::new(now, now + vec2!(x, y));
                    // 動けなかったらパス
                    if !course.can_move(&m) {
                        continue;
                    }
                    if 0 <= m.to.y && m.to.y < course.length as isize {
                        let c = 1.0 + costs[m.to.y as usize][m.to.x as usize];
                        if c < best {
                            best = c;
                        }
                    }
                }
            }
            best
        }

        while let Some(state) = heap.pop() {
            for nei in course.neighbors(state.pos) {
                let next_cost = search(course, &costs, nei);
                if next_cost < costs[nei.y as usize][nei.x as usize] {
                    costs[nei.y as usize][nei.x as usize] = next_cost;
                    heap.push(CostState {
                        cost: next_cost,
                        pos: nei,
                    })
                }
            }
        }

        CleverCost {
            width: course.width as isize,
            length: course.length as isize,
            costs,
        }
    }
}

impl CostMap for CleverCost {
    fn get_cost(&self, position: &Vec2) -> f64 {
        if position.x < 0 || self.width <= position.x {
            return f64::INFINITY;
        }
        if self.length <= position.y {
            return 0.0;
        }
        // これ微妙
        if position.y < 0 {
            let m = self.costs[0]
                .iter()
                .enumerate()
                .map(|(x, c)| {
                    let dx = (x as isize - position.x).abs();
                    dx as f64 + c
                })
                .fold(f64::NAN, f64::min);
            return m - position.y as f64;
        }
        self.costs[position.y as usize][position.x as usize]
    }
}

impl CostGrad for CleverCost {
    fn get_grad(&self, position: &Vec2) -> Vec2 {
        let x0 = self.get_cost(&(position + &vec2!(-1, 0)));
        let x1 = self.get_cost(&(position + &vec2!(1, 0)));
        let y0 = self.get_cost(&(position + &vec2!(0, -1)));
        let y1 = self.get_cost(&(position + &vec2!(0, 1)));
        let x = if x0 > x1 {
            1
        } else if x0 < x1 {
            -1
        } else {
            0
        };
        let y = if y0 > y1 {
            1
        } else if y0 < y1 {
            -1
        } else {
            0
        };
        vec2!(x, y)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::BufReader;

    #[test]
    fn naive_cost_map() {
        let mut input = BufReader::new(
            r#"0 0 0
1 1 2
0 2 0
0 0 1
"#
            .as_bytes(),
        );
        let costs = &[
            [7.0, 6.0, 5.0],
            [f64::INFINITY, f64::INFINITY, 4.0],
            [2.0, 2.0, 3.0],
            [1.0, 1.0, f64::INFINITY],
        ];
        let c = Course::from_reader(3, 4, &mut input).unwrap();
        let cm = NaiveCost::new(&c);
        assert_eq!(&cm.costs, costs);
        assert_eq!(cm.get_cost(&vec2!(0, 0)), 7.0);
        assert_eq!(cm.get_cost(&vec2!(0, -1)), 8.0);
        assert_eq!(cm.get_cost(&vec2!(-1, 0)), f64::INFINITY);
        assert_eq!(cm.get_cost(&vec2!(4, 0)), f64::INFINITY);
        assert_eq!(cm.get_cost(&vec2!(1, 10)), 0.0);
    }

    #[test]
    fn naive_cost_grad() {
        let mut input = BufReader::new(
            r#"0 0 0
0 1 1
0 2 0
0 2 0
0 0 1
"#
            .as_bytes(),
        );
        let costs = &[
            [5.0, 6.0, 7.0],
            [4.0, f64::INFINITY, f64::INFINITY],
            [3.0, 3.0, 4.0],
            [2.0, 2.0, 3.0],
            [1.0, 1.0, f64::INFINITY],
        ];
        let c = Course::from_reader(3, 5, &mut input).unwrap();
        let cm = NaiveCost::new(&c);
        assert_eq!(&cm.costs, costs);
        assert_eq!(cm.get_grad(&vec2!(1, 3)), vec2!(-1, 1));
        assert_eq!(cm.get_grad(&vec2!(1, 2)), vec2!(-1, 1));
        assert_eq!(cm.get_grad(&vec2!(0, 2)), vec2!(1, 1));
        assert_eq!(cm.get_grad(&vec2!(1, 0)), vec2!(-1, -1));
    }

    #[test]
    fn clever_cost_map() {
        let mut input = BufReader::new(
            r#"0 0 0
1 1 2
0 2 0
0 0 1
"#
            .as_bytes(),
        );
        let costs = &[
            [7.0, 6.0, 5.0],
            [f64::INFINITY, f64::INFINITY, 4.0],
            [2.0, 2.0, 3.0],
            [1.0, 1.0, f64::INFINITY],
        ];
        let c = Course::from_reader(3, 4, &mut input).unwrap();
        let cm = CleverCost::new(&c);
        assert_eq!(&cm.costs, costs);
    }

    #[test]
    fn clever_cost_grad() {
        let mut input = BufReader::new(
            r#"0 0 0
0 1 1
0 2 0
0 2 0
0 0 1
"#
            .as_bytes(),
        );
        let costs = &[
            [5.0, 6.0, 7.0],
            [4.0, f64::INFINITY, f64::INFINITY],
            [3.0, 3.0, 3.0],
            [2.0, 2.0, 3.0],
            [1.0, 1.0, f64::INFINITY],
        ];
        let c = Course::from_reader(3, 5, &mut input).unwrap();
        let cm = CleverCost::new(&c);
        assert_eq!(&cm.costs, costs);
        assert_eq!(cm.get_grad(&vec2!(1, 3)), vec2!(-1, 1));
        assert_eq!(cm.get_grad(&vec2!(1, 2)), vec2!(0, 1));
        assert_eq!(cm.get_grad(&vec2!(0, 2)), vec2!(1, 1));
        assert_eq!(cm.get_grad(&vec2!(1, 0)), vec2!(-1, -1));
    }
}
