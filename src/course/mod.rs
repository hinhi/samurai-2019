//! コース関連パッケージ
mod cost;
mod generator;

use std::fmt::{self, Display};
use std::io::{self, BufRead};
use std::isize;
use std::iter::Iterator;

use crate::vec2::{Movement, Vec2};

pub use self::cost::*;
pub use self::generator::*;

/// コース本体
///
/// 以下の生成パスが存在する
///
/// 1. ゲーム管理者のインプットから生成(``from_reader``)
/// 2. 現在の状態から未知部分をランダムに生成(``generator``で実装)
#[derive(Clone, Eq, PartialEq, Hash)]
pub struct Course {
    width: usize,
    length: usize,
    jimen: Vec<Vec<JimenType>>,
}

/// 地面の種類
///
/// それぞれ
/// 1. 平坦
/// 2. 障害
/// 3. 水
/// 4. 未知
#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash)]
enum JimenType {
    Heitan,
    Shogai,
    Mizu,
    Unknown,
}

impl<'a> From<&'a str> for JimenType {
    fn from(s: &'a str) -> JimenType {
        match s {
            "0" => JimenType::Heitan,
            "1" => JimenType::Shogai,
            "2" => JimenType::Mizu,
            "-1" => JimenType::Unknown,
            _ => unreachable!(),
        }
    }
}

/// 上下左右最大4箇所から移動可能な位置を返すイテレータ
pub struct Neighbor<'a> {
    course: &'a Course,
    pos: Vec2,
    n: usize,
}

// `usize`へのキャストを助ける奴ら
struct USize(usize);

impl From<usize> for USize {
    fn from(v: usize) -> USize {
        USize(v)
    }
}

impl From<isize> for USize {
    fn from(v: isize) -> USize {
        USize(v as usize)
    }
}

impl From<i32> for USize {
    fn from(v: i32) -> USize {
        USize(v as usize)
    }
}

impl<T> From<&T> for USize
where
    T: Into<usize> + Clone,
{
    fn from(v: &T) -> USize {
        USize(v.clone().into())
    }
}

impl Course {
    /// 新規インスタンス作成
    ///
    /// `jimen`のサイズが不正であるかチェックしない。
    /// こういった「使う側が気をつける」関数はunsafeにすべきだが、まあいいでしょう。
    fn new_no_check(jimen: Vec<Vec<JimenType>>) -> Course {
        let length = jimen.len();
        let width = jimen[0].len();
        Course {
            width,
            length,
            jimen,
        }
    }

    /// ゲーム管理のインプットからコース生成
    ///
    /// 幅と長さは別途与える必要がある。
    pub fn from_reader<R: BufRead>(width: usize, length: usize, r: &mut R) -> io::Result<Course> {
        let mut jimen = Vec::new();
        for (y, line) in (0..length).zip(r.lines()) {
            jimen.push(Vec::new());
            for word in line?.split_whitespace() {
                jimen[y].push(JimenType::from(word));
            }
            debug_assert_eq!(jimen[y].len(), width);
        }
        debug_assert_eq!(jimen.len(), length);
        Ok(Course {
            width,
            length,
            jimen,
        })
    }

    #[inline]
    fn get<X, Y>(&self, x: X, y: Y) -> JimenType
    where
        X: Into<USize>,
        Y: Into<USize>,
    {
        let x = x.into().0;
        let y = y.into().0;
        self.jimen[y][x]
    }

    #[inline]
    fn put<X, Y>(&mut self, x: X, y: Y, t: JimenType)
    where
        X: Into<USize>,
        Y: Into<USize>,
    {
        let x = x.into().0;
        let y = y.into().0;
        self.jimen[y][x] = t;
    }

    /// 移動可能か判断する
    ///
    /// 実行効率は高くないがチューニングするには時期が早い。
    pub fn can_move(&self, movement: &Movement) -> bool {
        // コースアウト
        if movement.to.x < 0 || self.width as isize <= movement.to.x {
            return false;
        }
        // 障害物との接触
        for touched in movement.touched_pos() {
            let x = touched.x as usize;
            if 0 <= touched.y
                && touched.y < self.length as isize
                && self.get(x, touched.y) == JimenType::Shogai
            {
                return false;
            }
        }
        true
    }

    /// 池ポチャ
    ///
    /// 本来`is_ikepocha`にすべきなんだろうが、このメソッドそのものがシャレなのでこれで良い。
    pub fn ikepocha(&self, pos: Vec2) -> bool {
        if pos.x < 0 || self.width as isize <= pos.x {
            return false;
        }
        if pos.y < 0 || self.length as isize <= pos.y {
            return false;
        }
        self.get(pos.x, pos.y) == JimenType::Mizu
    }

    /// 特定位置から到達可能な上下左右最大4個所の位置を返すイテレータを返す
    pub fn neighbors(&self, pos: Vec2) -> Neighbor {
        Neighbor {
            course: self,
            pos,
            n: 0,
        }
    }
}

impl Display for Course {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for row in self.jimen.iter() {
            let mut buf = String::with_capacity(self.width);
            for t in row {
                buf.extend(
                    match *t {
                        JimenType::Heitan => "0 ",
                        JimenType::Shogai => "1 ",
                        JimenType::Mizu => "2 ",
                        JimenType::Unknown => "-1 ",
                    }
                    .chars(),
                );
            }
            buf.push('\n');
            f.write_str(&buf)?;
        }
        Ok(())
    }
}

const DIR: [[isize; 2]; 4] = [[1, 0], [-1, 0], [0, 1], [0, -1]];

impl<'a> Iterator for Neighbor<'a> {
    type Item = Vec2;
    fn next(&mut self) -> Option<Self::Item> {
        for (i, a) in DIR.iter().enumerate().skip(self.n) {
            self.n = i + 1;
            let x = self.pos.x + a[0];
            if x < 0 || self.course.width as isize <= x {
                continue;
            }
            let y = self.pos.y + a[1];
            if y < 0 || self.course.length as isize <= y {
                continue;
            }
            if self.course.get(x, y) == JimenType::Shogai {
                continue;
            }
            return Some(Vec2::new(x, y));
        }
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::HashSet;
    use std::io::BufReader;
    use std::iter::FromIterator;

    #[test]
    fn jimen_type() {
        assert_eq!(JimenType::from("0"), JimenType::Heitan);
        assert_eq!(JimenType::from("1"), JimenType::Shogai);
        assert_eq!(JimenType::from("2"), JimenType::Mizu);
        assert_eq!(JimenType::from("-1"), JimenType::Unknown);
    }

    #[test]
    fn from_reader() {
        let mut input = BufReader::new("0 1\n0 2\n-1 -1\n".as_bytes());
        let c = Course::from_reader(2, 3, &mut input).unwrap();
        assert_eq!(c.width, 2);
        assert_eq!(c.length, 3);
        assert_eq!(c.get(0, 0), JimenType::Heitan);
        assert_eq!(c.get(1, 0), JimenType::Shogai);
        assert_eq!(c.get(0, 1), JimenType::Heitan);
        assert_eq!(c.get(1, 1), JimenType::Mizu);
        assert_eq!(c.get(0, 2), JimenType::Unknown);
        assert_eq!(c.get(1, 2), JimenType::Unknown);
    }

    #[test]
    fn new_no_check() {
        let jimen = vec![
            vec![JimenType::Heitan, JimenType::Heitan],
            vec![JimenType::Heitan, JimenType::Heitan],
            vec![JimenType::Heitan, JimenType::Heitan],
        ];
        let c = Course::new_no_check(jimen);
        assert_eq!(c.width, 2);
        assert_eq!(c.length, 3);
    }

    #[test]
    fn can_move() {
        let mut input = BufReader::new(
            r#"0 0 0
1 0 2
0 0 0
0 0 0
"#
            .as_bytes(),
        );
        let c = Course::from_reader(3, 4, &mut input).unwrap();
        let test_cases = &[
            (Movement::new(vec2!(0, 0), vec2!(0, 0)), true),
            (Movement::new(vec2!(0, 0), vec2!(2, 0)), true),
            (Movement::new(vec2!(0, 0), vec2!(-1, 0)), false),
            (Movement::new(vec2!(0, 0), vec2!(3, 0)), false),
            (Movement::new(vec2!(0, 0), vec2!(0, 1)), false),
            (Movement::new(vec2!(0, 0), vec2!(1, 1)), false),
            (Movement::new(vec2!(0, 0), vec2!(0, -1)), true),
            (Movement::new(vec2!(1, 0), vec2!(1, 100)), true),
        ];
        for case in test_cases {
            assert_eq!(c.can_move(&case.0), case.1);
        }
    }

    #[test]
    fn ikepocha() {
        let mut input = BufReader::new(
            r#"2 0 0
1 0 2
0 0 0
2 0 0
"#
            .as_bytes(),
        );
        let c = Course::from_reader(3, 4, &mut input).unwrap();
        let test_cases = &[
            (vec2!(0, 0), true),
            (vec2!(1, 0), false),
            (vec2!(2, 0), false),
            (vec2!(3, 0), false),
            (vec2!(-1, 0), false),
            (vec2!(0, 2), false),
            (vec2!(0, 3), true),
            (vec2!(0, 4), false),
        ];
        for case in test_cases {
            assert_eq!(c.ikepocha(case.0), case.1);
        }
    }

    #[test]
    fn neighbors() {
        let mut input = BufReader::new(
            r#"0 0 0
1 0 2
0 0 0
0 0 0
"#
            .as_bytes(),
        );
        let c = Course::from_reader(3, 4, &mut input).unwrap();
        let test_cases = &[
            (vec2!(0, 0), vec![vec2!(1, 0)]),
            (vec2!(1, 0), vec![vec2!(0, 0), vec2!(2, 0), vec2!(1, 1)]),
            (
                vec2!(1, 2),
                vec![vec2!(0, 2), vec2!(2, 2), vec2!(1, 1), vec2!(1, 3)],
            ),
            (vec2!(1, 1), vec![vec2!(2, 1), vec2!(1, 0), vec2!(1, 2)]),
        ];
        for case in test_cases {
            let nei_set: HashSet<Vec2> = HashSet::from_iter(c.neighbors(case.0));
            assert_eq!(nei_set, HashSet::from_iter(case.1.clone()));
        }
    }
}
