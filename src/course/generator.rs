//! コース予測器
use std::cmp::{max, min};
use std::collections::{HashMap, HashSet};

use super::{Course, JimenType};
use rand::{seq::SliceRandom, Rng};

use crate::vec2::Vec2;

lazy_static! {
    static ref SCALE_X: HashMap<(usize, usize, usize), usize> = {
        use std::io::{BufRead, BufReader};

        let mut dat = BufReader::new(include_bytes!("scale_x.dat").as_ref());
        let mut map = HashMap::new();
        for _ in 0..490 {
            let mut line = String::new();
            dat.read_line(&mut line).unwrap();
            let mut words = line.split_whitespace();
            let orig_w = words.next().unwrap().parse().unwrap();
            let w = words.next().unwrap().parse().unwrap();
            let x = words.next().unwrap().parse().unwrap();
            let orig_x = words.next().unwrap().parse().unwrap();
            map.insert((orig_w, w, x), orig_x);
        }
        map
    };
}

/// コースの横幅を広げて指定した幅にマッチさせる便利関数
///
/// 公式のコースジェネレータから移植した。
fn scale_block(course: &Course, width: usize, reverse: bool) -> Course {
    assert!(course.width <= width);
    let mut jimen = Vec::new();
    for orig_line in course.jimen.iter() {
        let mut line = Vec::new();
        for x in 0..width {
            let x = if reverse { width - 1 - x } else { x };
            let orig_x = match SCALE_X.get(&(course.width, width, x)) {
                Some(n) => *n,
                None => {
                    (x as f64 / (width - 1) as f64 * (course.width - 1) as f64).round() as usize
                }
            };
            line.push(orig_line[orig_x]);
        }
        jimen.push(line);
    }
    Course::new_no_check(jimen)
}

/// コースにブロックを置けるか検証する。
fn block_can_put(course: &Course, block: &Course, oy: usize) -> bool {
    if oy + block.length > course.length {
        return false;
    }
    for y in 0..block.length {
        for x in 0..block.width {
            let yy = oy + y;
            // 置きたい先が未知ならOK
            if course.get(x, yy) == JimenType::Unknown {
                // 1行全部未知だという仮定
                break;
            }
            // 置きたい先が同じならOK
            if course.get(x, yy) == block.get(x, y) {
                continue;
            }
            // ダメだった
            return false;
        }
    }
    true
}

/// コースにブロックを置く（上書き）
///
/// 事前に `block_can_put` で置けることをチェックする必要がある。
fn put_block(course: &mut Course, block: &Course, oy: usize) {
    for y in 0..block.length {
        for x in 0..block.width {
            let yy = oy + y;
            course.put(x, yy, block.get(x, y));
        }
    }
}

// 公式のロゴをコンパイル時にバイト列として埋め込む
// ``Course`` の生成は実行時に初めて ``LOGOS`` にアクセスしたときのみ行われる
lazy_static! {
    static ref LOGOS: Vec<Course> = {
        use std::io::BufReader;

        let list = &[
            (include_bytes!("logos/logo_ipsj_15.dat").as_ref(), 15, 7),
            (include_bytes!("logos/logo_ipsj_20.dat").as_ref(), 20, 7),
            (include_bytes!("logos/logo_ipsj_5.dat").as_ref(), 5, 10),
            (include_bytes!("logos/logo_ipsj_w_15.dat").as_ref(), 15, 9),
            (include_bytes!("logos/logo_samurai_10.dat").as_ref(), 10, 17),
            (include_bytes!("logos/logo_samurai_20.dat").as_ref(), 20, 7),
            (include_bytes!("logos/logo_samurai_5.dat").as_ref(), 5, 21),
        ];
        let mut logos = Vec::new();
        for &(bytes, width, length) in list {
            let mut input = BufReader::new(bytes);
            let c = Course::from_reader(width, length, &mut input).unwrap();
            logos.push(c);
        }
        logos
    };
}

// 公式のブロックパターン
lazy_static! {
    static ref BLOCKES: Vec<Course> = {
        use std::io::BufReader;

        let list = &[
            (include_bytes!("blocks/block_10.dat").as_ref(), 10, 3),
            (include_bytes!("blocks/cap_10.dat").as_ref(), 10, 7),
            (include_bytes!("blocks/end_10.dat").as_ref(), 10, 7),
            (include_bytes!("blocks/trap_10.dat").as_ref(), 10, 8),
            (include_bytes!("blocks/trap_15.dat").as_ref(), 15, 8),
            (include_bytes!("blocks/trap_20.dat").as_ref(), 20, 10),
            (include_bytes!("blocks/trap_5.dat").as_ref(), 5, 9),
            (include_bytes!("blocks/water_10.dat").as_ref(), 10, 6),
            (include_bytes!("blocks/wblock_10.dat").as_ref(), 10, 7),
        ];
        let mut blocks = Vec::new();
        for &(bytes, width, length) in list {
            let mut input = BufReader::new(bytes);
            let c = Course::from_reader(width, length, &mut input).unwrap();
            blocks.push(c);
        }
        blocks
    };
}

/// 公式の「ロゴ」用コンテナ
struct Logo {
    courses: Vec<Course>,
    max_length: usize,
}

impl Logo {
    fn new(width: usize) -> Logo {
        let courses = LOGOS
            .iter()
            .filter_map(|c| {
                if c.width > width {
                    return None;
                }
                Some(scale_block(c, width, false))
            })
            .collect::<Vec<_>>();
        let max_length = courses.iter().map(|c| c.length).max().unwrap();
        Logo {
            courses,
            max_length,
        }
    }
    fn shuffled<R: Rng>(&self, r: &mut R) -> Vec<&Course> {
        let mut c: Vec<_> = self.courses.iter().collect();
        c.shuffle(r);
        c
    }
}

/// 公式の「パターン」用コンテナ
struct Pattern {
    courses: Vec<Course>,
}

impl Pattern {
    fn new(width: usize) -> Pattern {
        let mut courses = Vec::new();
        for course in BLOCKES.iter() {
            if course.width > width {
                continue;
            }
            courses.push(scale_block(course, width, false));
            courses.push(scale_block(course, width, true));
        }
        Pattern { courses }
    }
    fn shuffled<R: Rng>(&self, r: &mut R) -> Vec<&Course> {
        let mut c: Vec<_> = self.courses.iter().collect();
        c.shuffle(r);
        c
    }
}

fn gen_pond<R: Rng>(r: &mut R, width: usize, max_length: usize) -> Course {
    let length = r.gen_range(1, max_length + 1);
    let mut c = Course::new_no_check(vec![vec![JimenType::Mizu; width]; length]);
    if r.gen() {
        // 非対称な場合
        let rate = r.gen_range(0.0, 0.3);
        for _ in 0..((width * length) as f64 * rate).floor() as usize {
            let x = r.gen_range(0, width);
            let y = r.gen_range(0, length);
            c.put(x, y, JimenType::Heitan);
        }
    } else {
        // 対称な場合
        let rate = r.gen_range(0.0, 0.15);
        for _ in 0..((width * length) as f64 * rate).floor() as usize {
            let x = r.gen_range(0, width);
            let y = r.gen_range(0, length);
            c.put(x, y, JimenType::Heitan);
            c.put(width - 1 - x, y, JimenType::Heitan);
        }
    }
    c
}

fn gen_g<R: Rng>(r: &mut R, width: usize) -> Course {
    let mut c = Course::new_no_check(vec![vec![JimenType::Heitan; width]; 2]);
    let t = if r.gen() {
        r.gen_range(0, 3)
    } else {
        r.gen_range(0, 5)
    };
    if t == 0 && 1 < width / 3 {
        let w = r.gen_range(1, width / 3);
        for x in w..width - w {
            c.put(x, 1, JimenType::Shogai);
        }
    } else if t == 1 && 2 < width / 3 {
        let w1 = r.gen_range(2, width / 3);
        let w2 = (width - w1) / 2;
        for x in 0..w2 {
            c.put(x, 1, JimenType::Shogai);
        }
        for x in w1 + w2..width {
            c.put(x, 1, JimenType::Shogai);
        }
    } else if t == 2 && 4 < width {
        let x1 = r.gen_range(1, width - 3);
        let x2 = if r.gen() {
            width - 2 - x1
        } else {
            r.gen_range(1, width - 3)
        };
        for x in 0..width {
            if x == x1 || x == x1 + 1 || x == x2 || x == x2 + 1 {
                continue;
            }
            c.put(x, 1, JimenType::Shogai);
        }
    } else if t == 3 && 2 < width / 3 {
        for x in r.gen_range(2, width / 3)..width {
            c.put(x, 1, JimenType::Shogai);
        }
    } else if t == 4 && 2 < width / 3 {
        for x in 0..width - r.gen_range(2, width / 3) {
            c.put(x, 1, JimenType::Shogai);
        }
    } else if 4 < width {
        let s = r.gen_range(0, width - 3);
        for x in 0..width {
            if x == s + 1 || x == s + 2 {
                continue;
            }
            c.put(x, 1, JimenType::Shogai)
        }
    }
    c
}

fn gen_one_rocks<R: Rng>(r: &mut R, width: usize) -> Course {
    let length = r.gen_range(2, 6);
    let mut c = Course::new_no_check(vec![vec![JimenType::Heitan; width]; length]);
    if r.gen() {
        let n = ((width * length) as f64 * r.gen_range(0.1, 0.9)).floor() as usize;
        for _ in 0..n {
            let x = r.gen_range(0, width);
            // 1以上であることに注意
            let y = r.gen_range(1, length);
            c.put(x, y, JimenType::Shogai);
        }
    } else {
        let n = ((width * length) as f64 * r.gen_range(0.05, 0.45)).floor() as usize;
        for _ in 0..n {
            let x = r.gen_range(0, width);
            // 1以上であることに注意
            let y = r.gen_range(1, length);
            c.put(x, y, JimenType::Shogai);
            c.put(width - 1 - x, y, JimenType::Shogai);
        }
    }
    c
}

fn gen_areas<R: Rng>(r: &mut R, width: usize) -> Course {
    let length = r.gen_range(3, 10);
    let mut c = Course::new_no_check(vec![vec![JimenType::Heitan; width]; length]);
    let symmetric = r.gen();

    fn make_shape<R: Rng>(r: &mut R, width: usize, length: usize) -> Vec<Vec<bool>> {
        let mut shape = vec![vec![true; width]; length];
        let half = length / 2;
        for dy in &[-1, 1] {
            let mut y = half as isize + dy;
            let mut x_low = 0_isize;
            let mut x_high = width as isize;
            while 0 <= y && y < length as isize {
                let d = (y - half as isize).abs() as f64 / (length - half) as f64;
                let d = (1.0 - d * d).sqrt() / 2.0;
                x_low += (0.5 + r.gen_range(0.0, 1.0) * width as f64 * d).floor() as isize;
                x_high -= (0.5 + r.gen_range(0.0, 1.0) * width as f64 * d).floor() as isize;
                for x in 0..min(width as isize, x_low) {
                    shape[y as usize][x as usize] = false;
                }
                for x in max(0, x_high)..width as isize {
                    shape[y as usize][x as usize] = false;
                }
                y += dy;
            }
        }
        shape
    }

    fn put_shape<R: Rng>(
        r: &mut R,
        c: &mut Course,
        shape: Vec<Vec<bool>>,
        t: JimenType,
        symmetric: bool,
    ) {
        let sw = shape[0].len();
        let sl = shape.len();
        let ox = r.gen_range(-(sw as isize / 4), c.width as isize - (sw as isize / 4));
        let oy = r.gen_range(-(sl as isize / 4), c.length as isize - (sl as isize / 4));
        for y in 0..sl {
            let yy = oy + y as isize;
            if yy < 1 || c.length as isize <= yy {
                continue;
            }
            for x in 0..sw {
                let xx = ox + x as isize;
                if xx < 0 || c.width as isize <= xx {
                    continue;
                }
                if !shape[y][x] {
                    continue;
                }
                c.put(xx, yy, t);
                if symmetric {
                    c.put(c.width - 1 - xx as usize, yy, t);
                }
            }
        }
    }

    let mut n_r = r.gen_range(2, 5);
    if symmetric {
        n_r = (n_r + 1) / 2;
    }
    for _ in 0..n_r {
        let sw = r.gen_range(width / 6, width / 2);
        let sl = r.gen_range(2, length + 1);
        let shape = make_shape(r, sw, sl);
        put_shape(r, &mut c, shape, JimenType::Shogai, symmetric);
    }
    // NOTE 公式のミスでここも`n_r`を使ってる
    for _ in 0..n_r {
        let sw = r.gen_range(width / 8, width / 4);
        let sl = r.gen_range(2, length + 1);
        let shape = make_shape(r, sw, sl);
        put_shape(r, &mut c, shape, JimenType::Mizu, symmetric);
    }
    c.length += 1;
    c.jimen.insert(0, vec![JimenType::Heitan; width]);
    c
}

fn check_block(course: &Course) -> bool {
    let mut visited = vec![vec![false; course.width]; course.length];
    let mut stack = Vec::new();
    for x in 0..course.width {
        let y = course.length - 1;
        if course.get(x, y) != JimenType::Shogai {
            stack.push(Vec2::new(x as isize, y as isize));
            visited[y][x] = true;
        }
    }
    while !stack.is_empty() {
        let pos = stack.pop().unwrap();
        for n in course.neighbors(pos) {
            if n.y == 0 {
                return true;
            }
            if visited[n.y as usize][n.x as usize] {
                continue;
            }
            visited[n.y as usize][n.x as usize] = true;
            stack.push(n);
        }
    }
    false
}

fn next_block<R: Rng>(r: &mut R, width: usize) -> Course {
    loop {
        let t = r.gen_range(0, 4);
        if t == 0 {
            return gen_pond(r, width, 4);
        } else if t == 1 {
            return gen_g(r, width);
        } else if t == 2 {
            let c = gen_areas(r, width);
            if check_block(&c) {
                return c;
            }
        } else {
            let c = gen_one_rocks(r, width);
            if check_block(&c) {
                return c;
            }
        }
    }
}

/// 予測コース生成器のインターフェース
pub trait CourseGenerator {
    /// 現状のコースからコース全体像を予測し、候補を複数生成する
    ///
    /// レスポンスは`max_count`以下の要素数であることが求められるが（人間が担保する必要あり）、
    /// 空であっても問題ない。
    /// 使う側ではレスポンスが空である場合の対応を行わないと事故る可能性が高い。
    fn generate<R: Rng>(&self, r: &mut R, course: &Course, max_count: usize) -> Vec<Course>;
}

/// 気合の生成器
pub struct KiaiGenerator {
    logos: Logo,
    blocks: Pattern,
}

/// コース生成の補助データ構造
///
/// 長さ6のコースでの例（`o`が判明済み、`#`が不明）
///
/// ```text
/// 6     <- back
/// 5 ###
/// 4 ###
/// 3 ###
/// 2 ### <- front
/// 1 ooo
/// 0 ooo
/// ```
#[derive(Clone)]
struct GenCourse {
    course: Course,
    front: usize,
    back: usize,
}

impl GenCourse {
    fn search_unknown(course: &Course, front_min: usize, back_max: usize) -> (usize, usize) {
        let mut front = back_max;
        for y in front_min..back_max {
            if course.get(0, y) == JimenType::Unknown {
                front = y;
                break;
            }
        }
        let mut back = back_max;
        for y in front..back_max {
            if course.get(0, y) != JimenType::Unknown {
                back = y;
                break;
            }
        }
        (front, back)
    }

    fn new(course: &Course) -> GenCourse {
        let (front, back) = GenCourse::search_unknown(course, 0, course.length);
        GenCourse {
            course: course.clone(),
            front,
            back,
        }
    }

    fn unknown_len(&self) -> usize {
        self.back - self.front
    }

    fn put(&mut self, course: &Course, oy: usize) {
        put_block(&mut self.course, course, oy);
        // 効率は忘れる
        let (front, back) = GenCourse::search_unknown(&self.course, self.front, self.back);
        self.front = front;
        self.back = back;
    }
}

impl KiaiGenerator {
    pub fn new(width: usize) -> KiaiGenerator {
        KiaiGenerator {
            logos: Logo::new(width),
            blocks: Pattern::new(width),
        }
    }

    /// 参照を返すイテレータから値を取得し1つ置けるまでやる
    /// 置けたら`true`を返し、置けずにイテレータが尽きたら`false`を返す
    fn put_iter<'a, I>(&self, gen_c: &mut GenCourse, courses: I, oy: usize) -> bool
    where
        I: IntoIterator<Item = &'a Course>,
    {
        for c in courses {
            if oy + c.length <= gen_c.course.length && block_can_put(&gen_c.course, c, oy) {
                gen_c.put(c, oy);
                return true;
            }
        }
        false
    }

    fn put_one(&self, gen_c: &mut GenCourse, course: Course, oy: usize) -> bool {
        if oy + course.length <= gen_c.course.length && block_can_put(&gen_c.course, &course, oy) {
            gen_c.put(&course, oy);
            return true;
        }
        false
    }

    fn put_start_logo(&self, gen_c: &GenCourse) -> Vec<GenCourse> {
        let mut ret = Vec::new();
        if gen_c.front >= self.logos.max_length {
            return ret;
        }
        // スタート側
        for logo in &self.logos.courses {
            if gen_c.course.length < logo.length {
                continue;
            }
            if !block_can_put(&gen_c.course, logo, 0) {
                continue;
            }
            let mut cloned = gen_c.clone();
            cloned.put(logo, 0);
            ret.push(cloned);
        }
        ret
    }

    fn put_goal_logo(&self, gen_c: &GenCourse) -> Vec<GenCourse> {
        let mut ret = Vec::new();
        if gen_c.unknown_len() == 0 {
            return ret;
        }
        for logo in &self.logos.courses {
            if gen_c.course.length < logo.length {
                continue;
            }
            let oy = gen_c.course.length - logo.length;
            if !block_can_put(&gen_c.course, logo, oy) {
                continue;
            }
            let mut cloned = gen_c.clone();
            cloned.put(logo, oy);
            ret.push(cloned);
        }
        ret
    }

    fn fill_all<R: Rng>(&self, r: &mut R, gen_c: &mut GenCourse) {
        if gen_c.unknown_len() <= 1 {
            return;
        }
        'OUT: for c in self
            .blocks
            .shuffled(r)
            .iter()
            .chain(self.logos.shuffled(r).iter())
        {
            for dy in 3..c.length - 1 {
                if dy + 7 >= gen_c.front {
                    break;
                }
                if block_can_put(&gen_c.course, c, gen_c.front - dy) {
                    put_block(&mut gen_c.course, c, gen_c.front - dy);
                    break 'OUT;
                }
            }
        }
        // 不明部分が1行以下になるまでやる
        // 何かを置こうとして置けなかったてもそこで終わり（公式準拠）
        while gen_c.unknown_len() >= 1 {
            if r.gen_range(0.0, 1.0) > 0.9 {
                if !self.put_iter(gen_c, self.logos.shuffled(r), gen_c.front) {
                    break;
                }
            } else if r.gen_range(0.0, 1.0) > 0.7 {
                if !self.put_iter(gen_c, self.blocks.shuffled(r), gen_c.front) {
                    break;
                }
            } else {
                let c = next_block(r, gen_c.course.width);
                if !self.put_one(gen_c, c, gen_c.front) {
                    break;
                }
            }
        }
    }
}

impl CourseGenerator for KiaiGenerator {
    fn generate<R: Rng>(&self, r: &mut R, course: &Course, max_count: usize) -> Vec<Course> {
        let gen_c = GenCourse::new(course);
        let mut start_put = self.put_start_logo(&gen_c);
        if start_put.is_empty() {
            start_put = vec![gen_c];
        }
        let mut goal_put = Vec::new();
        for gen_c in start_put.iter() {
            goal_put.extend(self.put_goal_logo(gen_c))
        }
        if goal_put.is_empty() {
            goal_put = start_put;
        }
        let max_count = if max_count == 0 { 1 } else { max_count };
        let mut h = HashSet::with_capacity(max_count);
        while h.len() < max_count {
            let mut gen_c = goal_put.choose(r).unwrap().clone();
            self.fill_all(r, &mut gen_c);
            // 重複が出たら終わり
            if !h.insert(gen_c.course) {
                break;
            }
        }
        h.drain().collect()
    }
}

#[cfg(test)]
mod tests {
    use std::io::BufReader;

    use rand::FromEntropy;
    use rand_xorshift::XorShiftRng;

    use super::*;

    #[test]
    fn logos_read() {
        // LOGOSに触ってロードさせることが目的
        for logo in LOGOS.iter() {
            assert_eq!(logo.jimen.len(), logo.length);
            assert_eq!(logo.jimen[0].len(), logo.width);
        }
    }

    #[test]
    fn blocks_read() {
        // LOGOSに触ってロードさせることが目的
        for block in BLOCKES.iter() {
            assert_eq!(block.jimen.len(), block.length);
            assert_eq!(block.jimen[0].len(), block.width);
        }
    }

    #[test]
    fn test_scale_block() {
        let mut input = BufReader::new(
            r#"2 0 0
1 0 2
"#
            .as_bytes(),
        );
        let c = Course::from_reader(3, 2, &mut input).unwrap();

        let cc = scale_block(&c, 3, false);
        assert_eq!(cc.width, 3);
        assert_eq!(cc.get(0, 0), JimenType::Mizu);
        assert_eq!(cc.get(1, 0), JimenType::Heitan);
        assert_eq!(cc.get(2, 0), JimenType::Heitan);
        assert_eq!(cc.get(0, 1), JimenType::Shogai);
        assert_eq!(cc.get(1, 1), JimenType::Heitan);
        assert_eq!(cc.get(2, 1), JimenType::Mizu);

        let cc = scale_block(&c, 4, false);
        assert_eq!(cc.width, 4);
        assert_eq!(cc.get(0, 0), JimenType::Mizu);
        assert_eq!(cc.get(1, 0), JimenType::Heitan);
        assert_eq!(cc.get(2, 0), JimenType::Heitan);
        assert_eq!(cc.get(3, 0), JimenType::Heitan);
        assert_eq!(cc.get(0, 1), JimenType::Shogai);
        assert_eq!(cc.get(1, 1), JimenType::Heitan);
        assert_eq!(cc.get(2, 1), JimenType::Heitan);
        assert_eq!(cc.get(3, 1), JimenType::Mizu);

        let cc = scale_block(&c, 6, false);
        assert_eq!(cc.width, 6);
        assert_eq!(cc.get(0, 0), JimenType::Mizu);
        assert_eq!(cc.get(1, 0), JimenType::Mizu);
        assert_eq!(cc.get(2, 0), JimenType::Heitan);
        assert_eq!(cc.get(3, 0), JimenType::Heitan);
        assert_eq!(cc.get(4, 0), JimenType::Heitan);
        assert_eq!(cc.get(5, 0), JimenType::Heitan);
        assert_eq!(cc.get(0, 1), JimenType::Shogai);
        assert_eq!(cc.get(1, 1), JimenType::Shogai);
        assert_eq!(cc.get(2, 1), JimenType::Heitan);
        assert_eq!(cc.get(3, 1), JimenType::Heitan);
        assert_eq!(cc.get(4, 1), JimenType::Mizu);
        assert_eq!(cc.get(5, 1), JimenType::Mizu);
    }

    #[test]
    fn test_scale_block2() {
        let mut input = BufReader::new(
            r#"0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 2 0 2 0 1 0 1 0 2
0 2 2 0 0 1 1 1 0 2
0 2 0 2 0 1 0 1 0 2
0 2 2 0 0 0 1 0 0 2
0 0 0 0 0 0 0 0 0 0
1 0 0 0 1 0 2 2 2 2
1 0 0 0 1 0 2 0 0 2
1 0 1 0 1 0 2 0 0 2
1 1 0 1 1 0 2 0 0 2
0 0 0 0 0 0 0 0 0 0
0 0 2 2 2 0 0 0 0 0
0 0 0 0 0 2 1 0 0 1
0 0 2 2 2 0 1 1 1 1
0 2 0 0 0 0 1 0 0 1
0 0 2 2 2 0 0 1 1 0
"#
            .as_bytes(),
        );
        let c = Course::from_reader(10, 17, &mut input).unwrap();
        let cc = scale_block(&c, 17, false);
        let mut input = BufReader::new(
            r#"0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
0 2 2 0 0 2 2 0 0 1 0 0 1 1 0 0 2
0 2 2 2 2 0 0 0 0 1 1 1 1 1 0 0 2
0 2 2 0 0 2 2 0 0 1 0 0 1 1 0 0 2
0 2 2 2 2 0 0 0 0 0 1 1 0 0 0 0 2
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
1 0 0 0 0 0 0 1 1 0 2 2 2 2 2 2 2
1 0 0 0 0 0 0 1 1 0 2 2 0 0 0 0 2
1 0 0 1 1 0 0 1 1 0 2 2 0 0 0 0 2
1 1 1 0 0 1 1 1 1 0 2 2 0 0 0 0 2
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 2 2 2 2 2 2 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 2 1 1 0 0 0 0 1
0 0 0 2 2 2 2 2 2 0 1 1 1 1 1 1 1
0 2 2 0 0 0 0 0 0 0 1 1 0 0 0 0 1
0 0 0 2 2 2 2 2 2 0 0 0 1 1 1 1 0
"#
            .as_bytes(),
        );
        let ans = Course::from_reader(17, 17, &mut input).unwrap();
        for y in 0..ans.length {
            for x in 0..ans.width {
                assert_eq!((x, y, ans.get(x, y)), (x, y, cc.get(x, y)));
            }
        }
    }
    #[test]
    fn test_scale_block_reverse() {
        let mut input = BufReader::new(
            r#"2 0 0
1 0 2
"#
            .as_bytes(),
        );
        let c = Course::from_reader(3, 2, &mut input).unwrap();

        let cc = scale_block(&c, 3, true);
        assert_eq!(cc.width, 3);
        assert_eq!(cc.get(2, 0), JimenType::Mizu);
        assert_eq!(cc.get(1, 0), JimenType::Heitan);
        assert_eq!(cc.get(0, 0), JimenType::Heitan);
        assert_eq!(cc.get(2, 1), JimenType::Shogai);
        assert_eq!(cc.get(1, 1), JimenType::Heitan);
        assert_eq!(cc.get(0, 1), JimenType::Mizu);

        let cc = scale_block(&c, 4, true);
        assert_eq!(cc.width, 4);
        assert_eq!(cc.get(3, 0), JimenType::Mizu);
        assert_eq!(cc.get(2, 0), JimenType::Heitan);
        assert_eq!(cc.get(1, 0), JimenType::Heitan);
        assert_eq!(cc.get(0, 0), JimenType::Heitan);
        assert_eq!(cc.get(3, 1), JimenType::Shogai);
        assert_eq!(cc.get(2, 1), JimenType::Heitan);
        assert_eq!(cc.get(1, 1), JimenType::Heitan);
        assert_eq!(cc.get(0, 1), JimenType::Mizu);

        let cc = scale_block(&c, 6, true);
        assert_eq!(cc.width, 6);
        assert_eq!(cc.get(5, 0), JimenType::Mizu);
        assert_eq!(cc.get(4, 0), JimenType::Mizu);
        assert_eq!(cc.get(3, 0), JimenType::Heitan);
        assert_eq!(cc.get(2, 0), JimenType::Heitan);
        assert_eq!(cc.get(1, 0), JimenType::Heitan);
        assert_eq!(cc.get(0, 0), JimenType::Heitan);
        assert_eq!(cc.get(5, 1), JimenType::Shogai);
        assert_eq!(cc.get(4, 1), JimenType::Shogai);
        assert_eq!(cc.get(3, 1), JimenType::Heitan);
        assert_eq!(cc.get(2, 1), JimenType::Heitan);
        assert_eq!(cc.get(1, 1), JimenType::Mizu);
        assert_eq!(cc.get(0, 1), JimenType::Mizu);
    }

    #[test]
    fn test_gen_pond() {
        let mut r = XorShiftRng::from_entropy();
        for width in 5..20 {
            let c = gen_pond(&mut r, width, 4);
            assert_eq!(c.width, width);
            assert!(c.length > 0);
            assert!(c.length <= 4);
            let n = c.length * c.width;
            let mut hn = 0; // 平坦の数
            let mut pn = 0; // 池の数
            for y in 0..c.length {
                for x in 0..c.width {
                    match c.get(x, y) {
                        JimenType::Heitan => hn += 1,
                        JimenType::Mizu => pn += 1,
                        _ => (),
                    }
                }
            }
            assert_eq!(hn + pn, n);
            assert!(pn <= n * 10 / 3);
            assert!(pn > 0);
        }
    }

    #[test]
    fn test_gen_g() {
        let mut r = XorShiftRng::from_entropy();
        for width in 5..20 {
            let c = gen_g(&mut r, width);
            assert_eq!(c.width, width);
            assert_eq!(c.length, 2);
            let n = (0..c.width)
                .filter(|x| c.get(x, 1) == JimenType::Heitan)
                .count();
            assert!(n > 0);
            assert!(n < c.width);
        }
    }

    #[test]
    fn test_gen_one_rocks() {
        let mut r = XorShiftRng::from_entropy();
        for width in 5..20 {
            let c = gen_one_rocks(&mut r, width);
            assert_eq!(c.width, width);
            assert!(c.length > 0);
            assert!(c.length <= 5);
            assert!((0..c.width).all(|x| c.get(x, 0) == JimenType::Heitan));
        }
    }

    #[test]
    fn test_gen_areas() {
        let mut r = XorShiftRng::from_entropy();
        // なんとなく10回くらいわまして配列外アクセスとか見つかりやすくする
        for _ in 0..10 {
            for width in 5..20 {
                let c = gen_areas(&mut r, width);
                assert_eq!(c.width, width);
                assert!(c.length > 0);
                assert!(c.length <= 11);
                assert!((0..c.width).all(|x| c.get(x, 0) == JimenType::Heitan));
            }
        }
    }

    #[test]
    fn test_check_block() {
        let c = Course::new_no_check(vec![vec![JimenType::Heitan; 5], vec![JimenType::Heitan; 5]]);
        assert_eq!(check_block(&c), true);
        let c = Course::new_no_check(vec![vec![JimenType::Heitan; 5], vec![JimenType::Shogai; 5]]);
        assert_eq!(check_block(&c), false);
        let c = Course::new_no_check(vec![
            vec![JimenType::Heitan; 5],
            vec![JimenType::Shogai; 5],
            vec![JimenType::Heitan; 5],
        ]);
        assert_eq!(check_block(&c), false);
        let c = Course::new_no_check(vec![
            vec![JimenType::Heitan; 5],
            vec![
                JimenType::Shogai,
                JimenType::Shogai,
                JimenType::Shogai,
                JimenType::Shogai,
                JimenType::Heitan,
            ],
            vec![JimenType::Heitan; 5],
            vec![
                JimenType::Heitan,
                JimenType::Shogai,
                JimenType::Shogai,
                JimenType::Shogai,
                JimenType::Shogai,
            ],
        ]);
        assert_eq!(check_block(&c), true);
    }

    #[test]
    fn test_next_block() {
        let mut r = XorShiftRng::from_entropy();
        for _ in 0..10 {
            for width in 5..20 {
                let c = next_block(&mut r, width);
                assert_eq!(check_block(&c), true);
            }
        }
    }

    #[test]
    fn gen_c_new() {
        let c = Course::new_no_check(vec![
            vec![JimenType::Heitan, JimenType::Heitan],
            vec![JimenType::Unknown, JimenType::Unknown],
            vec![JimenType::Unknown, JimenType::Unknown],
        ]);
        let gen_c = GenCourse::new(&c);
        assert_eq!(gen_c.front, 1);
        assert_eq!(gen_c.back, 3);
        assert_eq!(gen_c.unknown_len(), 2);

        let c = Course::new_no_check(vec![
            vec![JimenType::Heitan, JimenType::Heitan],
            vec![JimenType::Heitan, JimenType::Heitan],
            vec![JimenType::Unknown, JimenType::Unknown],
            vec![JimenType::Unknown, JimenType::Unknown],
            vec![JimenType::Mizu, JimenType::Heitan],
            vec![JimenType::Mizu, JimenType::Heitan],
        ]);
        let gen_c = GenCourse::new(&c);
        assert_eq!(gen_c.front, 2);
        assert_eq!(gen_c.back, 4);
        assert_eq!(gen_c.unknown_len(), 2);
    }

    #[test]
    fn gen_c_put() {
        let c = Course::new_no_check(vec![
            vec![JimenType::Heitan, JimenType::Heitan],
            vec![JimenType::Heitan, JimenType::Heitan],
            vec![JimenType::Unknown, JimenType::Unknown],
            vec![JimenType::Unknown, JimenType::Unknown],
            vec![JimenType::Unknown, JimenType::Unknown],
            vec![JimenType::Mizu, JimenType::Heitan],
            vec![JimenType::Mizu, JimenType::Heitan],
        ]);
        let mut gen_c = GenCourse::new(&c);
        let c2 = Course::new_no_check(vec![
            vec![JimenType::Shogai, JimenType::Heitan],
            vec![JimenType::Heitan, JimenType::Mizu],
        ]);
        gen_c.put(&c2, 2);
        assert_eq!(gen_c.front, 4);
        assert_eq!(gen_c.back, 5);
        assert_eq!(gen_c.unknown_len(), 1);
        gen_c.put(&c2, 3);
        assert_eq!(gen_c.front, 5);
        assert_eq!(gen_c.back, 5);
        assert_eq!(gen_c.unknown_len(), 0);
    }
}
