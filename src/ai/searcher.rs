use std::time::Duration;

use super::SearcherFactory;
use crate::searcher::{BeamSearcher, ChokudaiSearcher};

/// `ChokudaiSearcher`ファクトリ
pub struct ChokudaiSearcherFactory {
    max_depth: usize,
    beam_width: usize,
}

impl ChokudaiSearcherFactory {
    pub fn new(max_depth: usize, beam_width: usize) -> Self {
        ChokudaiSearcherFactory {
            max_depth,
            beam_width,
        }
    }
}

impl SearcherFactory for ChokudaiSearcherFactory {
    type Searcher = ChokudaiSearcher;
    fn create(&self, max_duration: Duration) -> Self::Searcher {
        ChokudaiSearcher::new(self.max_depth, self.beam_width, max_duration * 8 / 10)
    }
}

/// `BeamSearcher`ファクトリ
pub struct BeamSearcherFactory {
    max_depth: usize,
    beam_width: usize,
}

impl BeamSearcherFactory {
    pub fn new(max_depth: usize, beam_width: usize) -> Self {
        BeamSearcherFactory {
            max_depth,
            beam_width,
        }
    }
}

impl SearcherFactory for BeamSearcherFactory {
    type Searcher = BeamSearcher;
    fn create(&self, max_duration: Duration) -> Self::Searcher {
        BeamSearcher::new(self.max_depth, self.beam_width, max_duration)
    }
}
