//! AI制御系の関連パッケージ
mod ai;
mod cost;
mod searcher;
mod utils;

use std::io::BufRead;
use std::time::Duration;

use rand::Rng;

use crate::course::{CostGrad, CostMap, Course};
use crate::searcher::Searcher;
use crate::vec2::Vec2;

pub use self::ai::*;
pub use self::cost::*;
pub use self::searcher::*;
pub use self::utils::interpolation;

/// 基本的なAIのインターフェース
pub trait AI {
    fn planing<R, Rn>(&self, reader: &mut R, random: &mut Rn) -> Option<Vec2>
    where
        R: BufRead,
        Rn: Rng;
}

/// `Searcher`のファクトリインターフェース
pub trait SearcherFactory {
    type Searcher: Searcher;
    fn create(&self, max_duration: Duration) -> Self::Searcher;
}

/// 評価関数のファクトリインターフェース
pub trait CostFactory {
    type Cost: CostMap + CostGrad;
    fn create(&self, course: &Course) -> Self::Cost;
}

/// 相手の動きを予測するだけの機能のファクトリインターフェース
pub trait CostGradFactory {
    type Cost: CostGrad;
    fn create(&self, course: &Course) -> Self::Cost;
}
