use super::{CostFactory, CostGradFactory};

use crate::course::{CleverCost, CostGrad, Course, NaiveCost};
use crate::vec2::Vec2;

/// `NaiveCost`のファクトリ
pub struct NaiveCostFactory;

impl NaiveCostFactory {
    pub fn new() -> NaiveCostFactory {
        NaiveCostFactory {}
    }
}

impl CostFactory for NaiveCostFactory {
    type Cost = NaiveCost;
    fn create(&self, course: &Course) -> Self::Cost {
        NaiveCost::new(course)
    }
}

/// `CleverCost`のファクトリ
pub struct CleverCostFactory;

impl CleverCostFactory {
    pub fn new() -> CleverCostFactory {
        CleverCostFactory {}
    }
}

impl CostFactory for CleverCostFactory {
    type Cost = CleverCost;
    fn create(&self, course: &Course) -> Self::Cost {
        CleverCost::new(course)
    }
}

/// 相手の動きを定数として扱う
pub struct ConstGrad {
    acc: Vec2,
}

impl CostGrad for ConstGrad {
    fn get_grad(&self, _position: &Vec2) -> Vec2 {
        self.acc
    }
}

/// `ConstGrad`のファクトリ
pub struct ConstGradFactory {
    acc: Vec2,
}

impl ConstGradFactory {
    pub fn new(y: isize) -> ConstGradFactory {
        ConstGradFactory {
            acc: Vec2::new(0, y),
        }
    }
}

impl CostGradFactory for ConstGradFactory {
    type Cost = ConstGrad;
    fn create(&self, _course: &Course) -> Self::Cost {
        ConstGrad { acc: self.acc }
    }
}
