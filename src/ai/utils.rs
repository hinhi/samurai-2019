use std::cmp::Ord;
use std::collections::HashMap;

use crate::vec2::Vec2;

pub fn clip<T>(v: T, min_val: T, max_val: T) -> T
where
    T: Ord,
{
    if v < min_val {
        return min_val;
    }
    if v > max_val {
        return max_val;
    }
    v
}

pub fn calc_available_duration(
    course_length: usize,
    current_step: usize,
    current_pos: isize,
    rest_planning_time: usize,
) -> u64 {
    if current_step < 5 {
        300
    } else {
        let dy_per_step = if current_pos > 0 {
            current_pos as f64 / current_step as f64
        } else {
            1.0
        };
        let rest_length = course_length as f64 - current_pos as f64;
        let t = (rest_planning_time as f64 * dy_per_step / rest_length) as u64;
        t
    }
}

pub fn chose_best_by_avg(results: &[(Vec2, f64)]) -> Vec2 {
    let mut map = HashMap::new();
    for (acc, cost) in results {
        let x = map.entry(*acc).or_insert((0, 0.0));
        x.0 += 1;
        x.1 += cost;
    }
    // 平均が良いものを選ぶ
    let a = map
        .iter()
        .min_by(|a, b| {
            let a = (a.1).1 / f64::from((a.1).0);
            let b = (b.1).1 / f64::from((b.1).0);
            a.partial_cmp(&b).unwrap()
        })
        .unwrap()
        .0;
    *a
}

pub fn chose_best_by_frequency(results: &[(Vec2, f64)]) -> Vec2 {
    let mut map = HashMap::new();
    for (acc, cost) in results {
        let x = map.entry(*acc).or_insert((0, 0.0));
        x.0 += 1;
        x.1 += cost;
    }
    // 最頻かつ平均が良いものを選ぶ
    let a = map
        .iter()
        .max_by(|a, b| {
            let a = ((a.1).0, -(a.1).1 / f64::from((a.1).0));
            let b = ((b.1).0, -(b.1).1 / f64::from((b.1).0));
            a.partial_cmp(&b).unwrap()
        })
        .unwrap()
        .0;
    *a
}

pub fn interpolation(a: usize, b: usize, t: usize) -> usize {
    if b > a {
        a + (b - a) * t / 5
    } else {
        b + (a - b) * (5 - t) / 5
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_calc_available_duration() {
        assert_eq!(calc_available_duration(100, 0, 100, 10000), 300);
        assert_eq!(calc_available_duration(20, 5, 10, 1000), 200);
        assert_eq!(calc_available_duration(50, 16, 48, 100), 150);
    }

    #[test]
    fn test_clip() {
        assert_eq!(clip(-1, 0, 10), 0);
        assert_eq!(clip(0, 0, 10), 0);
        assert_eq!(clip(1, 0, 10), 1);
        assert_eq!(clip(10, 0, 10), 10);
        assert_eq!(clip(11, 0, 10), 10);
    }

    #[test]
    fn test_chose_best_by_avg() {
        let cases = &[
            (vec![(vec2!(3, 5), 0.0)], vec2!(3, 5)),
            (vec![(vec2!(3, 5), 1.0), (vec2!(0, 0), 0.9)], vec2!(0, 0)),
            (
                vec![(vec2!(2, 1), 1.0), (vec2!(1, 1), 1.1), (vec2!(2, 1), 1.3)],
                vec2!(1, 1),
            ),
            (
                vec![
                    (vec2!(-1, 1), 1.0),
                    (vec2!(1, 2), 1.2),
                    (vec2!(-1, 1), 1.2),
                    (vec2!(-1, 1), 1.1),
                ],
                vec2!(-1, 1),
            ),
        ];
        for case in cases.iter() {
            let a = chose_best_by_avg(&case.0);
            assert_eq!(a, case.1);
        }
    }

    #[test]
    fn test_chose_best_by_frequency() {
        let cases = &[
            (vec![(vec2!(3, 5), 0.0)], vec2!(3, 5)),
            (vec![(vec2!(3, 5), 1.0), (vec2!(0, 0), 0.9)], vec2!(0, 0)),
            (
                vec![(vec2!(2, 1), 1.0), (vec2!(1, 1), 0.0), (vec2!(2, 1), 1.3)],
                vec2!(2, 1),
            ),
            (
                vec![
                    (vec2!(-1, 1), 1.0),
                    (vec2!(1, 2), 1.2),
                    (vec2!(-1, 1), 1.2),
                    (vec2!(1, 2), 1.1),
                ],
                vec2!(-1, 1),
            ),
        ];
        for case in cases.iter() {
            let a = chose_best_by_frequency(&case.0);
            assert_eq!(a, case.1);
        }
    }

    #[test]
    fn test_interpolation() {
        assert_eq!(interpolation(0, 10, 0), 0);
        assert_eq!(interpolation(0, 10, 5), 10);
        assert_eq!(interpolation(0, 10, 2), 4);
        assert_eq!(interpolation(20, 10, 0), 20);
        assert_eq!(interpolation(20, 10, 5), 10);
        assert_eq!(interpolation(20, 10, 4), 12);
    }
}
