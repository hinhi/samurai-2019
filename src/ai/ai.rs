use std::io::BufRead;
use std::time::Duration;

use num_cpus;
use rand::{Rng, SeedableRng};
use rand_xorshift::XorShiftRng;
use rayon::prelude::*;

use super::utils::{calc_available_duration, chose_best_by_avg, chose_best_by_frequency, clip};
use crate::course::CourseGenerator;
use crate::race::{Race, SimulatedRaceStep};
use crate::searcher::Searcher;
use crate::vec2::Vec2;

use super::{CostFactory, CostGradFactory, SearcherFactory, AI};

pub struct NaiveAI<S, M, G> {
    race: Race,
    searcher_factory: S,
    cost_map_factory: M,
    cost_grad_factory: G,
}

impl<S, M, G> NaiveAI<S, M, G> {
    pub fn new(
        race: Race,
        searcher_factory: S,
        cost_map_factory: M,
        cost_grad_factory: G,
    ) -> NaiveAI<S, M, G> {
        NaiveAI {
            race,
            searcher_factory,
            cost_map_factory,
            cost_grad_factory,
        }
    }
}

impl<S, M, G> AI for NaiveAI<S, M, G>
where
    S: SearcherFactory,
    M: CostFactory,
    G: CostGradFactory,
{
    fn planing<R, Rn>(&self, reader: &mut R, random: &mut Rn) -> Option<Vec2>
    where
        R: BufRead,
        Rn: Rng,
    {
        let step = match self.race.step_from_reader(reader) {
            Ok(s) => s,
            Err(_) => return None,
        };
        let sim = step.to_simulated();
        let map = self.cost_map_factory.create(sim.course);
        let grad = self.cost_grad_factory.create(sim.course);
        let d = calc_available_duration(
            self.race.length,
            step.current_step,
            step.players[0].position.y,
            step.rest_planning_time,
        );
        let d = clip(d, 10, 1000);
        let searcher = self.searcher_factory.create(Duration::from_millis(d));
        Some(searcher.search(random, &map, &grad, &sim).0)
    }
}

/// シングルスレッド/評価関数と相手の予測が密結合のAI
pub struct SingleThreadAI<S, C, G> {
    race: Race,
    searcher_factory: S,
    cost_factory: C,
    course_generator: G,
}

impl<S, C, G> SingleThreadAI<S, C, G> {
    pub fn new(
        race: Race,
        searcher_factory: S,
        cost_factory: C,
        course_generator: G,
    ) -> SingleThreadAI<S, C, G> {
        SingleThreadAI {
            race,
            searcher_factory,
            cost_factory,
            course_generator,
        }
    }
}

impl<S, C, G> AI for SingleThreadAI<S, C, G>
where
    S: SearcherFactory,
    C: CostFactory,
    G: CourseGenerator,
{
    fn planing<R, Rn>(&self, reader: &mut R, random: &mut Rn) -> Option<Vec2>
    where
        R: BufRead,
        Rn: Rng,
    {
        let step = match self.race.step_from_reader(reader) {
            Ok(s) => s,
            Err(_) => return None,
        };
        let mut courses = self.course_generator.generate(random, &step.course, 1);
        if courses.is_empty() {
            courses = vec![step.course.clone()];
        }
        let sim = SimulatedRaceStep::new(&step, &courses[0]);
        let cost = self.cost_factory.create(sim.course);
        let d = calc_available_duration(
            self.race.length,
            step.current_step,
            step.players[0].position.y,
            step.rest_planning_time,
        );
        let d = clip(d, 10, 1000);
        let searcher = self.searcher_factory.create(Duration::from_millis(d));
        Some(searcher.search(random, &cost, &cost, &sim).0)
    }
}

/// マルチスレッド/評価関数と相手の予測が密結合のAI
pub struct MultiThreadAI<S, C, G> {
    cpus: usize,
    multiplier: usize,
    race: Race,
    searcher_factory: S,
    cost_factory: C,
    course_generator: G,
}

impl<S, C, G> MultiThreadAI<S, C, G> {
    pub fn new(
        multiplier: usize,
        race: Race,
        searcher_factory: S,
        cost_factory: C,
        course_generator: G,
    ) -> MultiThreadAI<S, C, G> {
        let cpus = num_cpus::get();
        eprintln!("cpus={}", cpus);
        MultiThreadAI {
            cpus,
            multiplier,
            race,
            searcher_factory,
            cost_factory,
            course_generator,
        }
    }
}

impl<S, C, G> AI for MultiThreadAI<S, C, G>
where
    S: SearcherFactory + Sync,
    C: CostFactory + Sync,
    G: CourseGenerator + Sync,
{
    fn planing<R, Rn>(&self, reader: &mut R, random: &mut Rn) -> Option<Vec2>
    where
        R: BufRead,
        Rn: Rng,
    {
        let step = match self.race.step_from_reader(reader) {
            Ok(s) => s,
            Err(_) => return None,
        };
        let mut courses =
            self.course_generator
                .generate(random, &step.course, self.cpus * self.multiplier);
        if courses.is_empty() {
            courses = vec![step.course.clone()];
        }
        let d = calc_available_duration(
            self.race.length,
            step.current_step,
            step.players[0].position.y,
            step.rest_planning_time,
        );
        let d = d / ((courses.len() + self.cpus - 1) / self.cpus) as u64;
        let d = Duration::from_millis(clip(d, 10, 1000));
        eprintln!(
            "step={} courses={} d={:?}",
            step.current_step,
            courses.len(),
            d
        );
        // マルチスレッドで処理
        let res = courses
            .iter()
            .zip((0..courses.len()).map(|_| random.next_u64()))
            .collect::<Vec<_>>()
            .par_iter()
            .map(|(course, seed)| {
                let mut random = XorShiftRng::seed_from_u64(*seed);
                let searcher = self.searcher_factory.create(d);
                let sim = SimulatedRaceStep::new(&step, &course);
                let cost = self.cost_factory.create(sim.course);
                searcher.search(&mut random, &cost, &cost, &sim)
            })
            .collect::<Vec<_>>();
        // 数え上げ
        Some(chose_best_by_frequency(&res))
    }
}

/// マルチスレッド
pub struct MultiThreadGradAI<S, CM, CG, G> {
    cpus: usize,
    multiplier: usize,
    race: Race,
    searcher_factory: S,
    cost_map_factory: CM,
    cost_grad_factory: CG,
    course_generator: G,
}

impl<S, CM, CG, G> MultiThreadGradAI<S, CM, CG, G> {
    pub fn new(
        multiplier: usize,
        race: Race,
        searcher_factory: S,
        cost_map_factory: CM,
        cost_grad_factory: CG,
        course_generator: G,
    ) -> MultiThreadGradAI<S, CM, CG, G> {
        let cpus = num_cpus::get();
        eprintln!("cpus={}", cpus);
        MultiThreadGradAI {
            cpus,
            multiplier,
            race,
            searcher_factory,
            cost_map_factory,
            cost_grad_factory,
            course_generator,
        }
    }
}

impl<S, CM, CG, G> AI for MultiThreadGradAI<S, CM, CG, G>
where
    S: SearcherFactory + Sync,
    CM: CostFactory + Sync,
    CG: CostGradFactory + Sync,
    G: CourseGenerator + Sync,
{
    fn planing<R, Rn>(&self, reader: &mut R, random: &mut Rn) -> Option<Vec2>
    where
        R: BufRead,
        Rn: Rng,
    {
        let step = match self.race.step_from_reader(reader) {
            Ok(s) => s,
            Err(_) => return None,
        };
        let mut courses =
            self.course_generator
                .generate(random, &step.course, self.cpus * self.multiplier);
        if courses.is_empty() {
            courses = vec![step.course.clone()];
        }
        let d = calc_available_duration(
            self.race.length,
            step.current_step,
            step.players[0].position.y,
            step.rest_planning_time,
        );
        let d = d / ((courses.len() + self.cpus - 1) / self.cpus) as u64;
        let d = Duration::from_millis(clip(d, 10, 1000));
        eprintln!(
            "step={} courses={} d={:?}",
            step.current_step,
            courses.len(),
            d
        );
        // マルチスレッドで処理
        let res = courses
            .iter()
            .zip((0..courses.len()).map(|_| random.next_u64()))
            .collect::<Vec<_>>()
            .par_iter()
            .map(|(course, seed)| {
                let mut random = XorShiftRng::seed_from_u64(*seed);
                let searcher = self.searcher_factory.create(d);
                let sim = SimulatedRaceStep::new(&step, &course);
                let map = self.cost_map_factory.create(sim.course);
                let grad = self.cost_grad_factory.create(sim.course);
                searcher.search(&mut random, &map, &grad, &sim)
            })
            .collect::<Vec<_>>();
        // 数え上げ
        Some(chose_best_by_avg(&res))
    }
}
