use std::io::{self, BufRead};

use crate::course::Course;
use crate::player::Player;
use crate::vec2::{Movement, Vec2};

/// レース初期化時の情報を保持する
///
/// 基本的にAI初期化時に1度だけインスタンス化される。
pub struct Race {
    pub planning_time_limit: usize,
    pub step_limit: usize,
    pub width: usize,
    pub length: usize,
    pub vision: usize,
}

/// レースの各ステップでのゲーム管理者からのインプットのパース結果を保持する
///
/// 基本的に書くステップ開始時にインスタンス化される。
/// この構造体の使い道は多くなく、基本的に[`SimulatedRaceStep`]を生成することになると思われる。
///
/// [`SimulatedRaceStep`]: struct.SimulatedRaceStep.html
pub struct RaceStep<'a> {
    race: &'a Race,
    pub current_step: usize,
    pub rest_planning_time: usize,
    pub players: [Player; 2],
    pub course: Course,
}

/// AI用の内部ゲームシミュレートの結果状態を保持する
///
/// [`RaceStep`]から生成するか[`SimulatedRaceStep.simulate`]で生成する。
///
/// [`RaceStep`]: struct.RaceStep.html
/// [`SimulatedRaceStep.simulate`]: struct.SimulatedRaceStep.html#method.simulate
pub struct SimulatedRaceStep<'a> {
    race: &'a Race,
    pub current_step: usize,
    pub players: [Player; 2],
    pub course: &'a Course,
}

impl Race {
    /// ゲーム開始時の入力からインスタンスを生成する
    ///
    /// 不正な入力をチェックしていない。
    pub fn from_reader<R: BufRead>(r: &mut R) -> io::Result<Race> {
        let mut lines = r.lines().take(4);
        let planning_time_limit = lines.next().unwrap()?.parse().unwrap();
        let step_limit = lines.next().unwrap()?.parse().unwrap();
        let width_length = lines.next().unwrap()?;
        let mut nums = width_length.split_whitespace();
        let width = nums.next().unwrap().parse().unwrap();
        let length = nums.next().unwrap().parse().unwrap();
        let vision = lines.next().unwrap()?.parse().unwrap();
        Ok(Race {
            planning_time_limit,
            step_limit,
            width,
            length,
            vision,
        })
    }

    /// 各ターン開始時の入力から[`RaceStep`]インスタンスを作成する
    ///
    /// ゲームが正常終了した場合、全体として唯一このメソッドで`EOF`を検出することになる。
    /// よって、呼び出し側ではこのメソッドの返り値だけはチェックした方がいい。
    ///
    /// [`RaceStep`]: struct.RaceStep.html
    pub fn step_from_reader<R: BufRead>(&self, r: &mut R) -> io::Result<RaceStep> {
        let mut lines = r.lines().take(2);
        let current_step = match lines.next() {
            Some(line) => line?.parse().unwrap(),
            None => {
                return Err(io::Error::new(io::ErrorKind::UnexpectedEof, "EOF"));
            }
        };
        let rest_planning_time = lines.next().unwrap()?.parse().unwrap();
        let p_self = Player::from_reader(self.length, r)?;
        let p_other = Player::from_reader(self.length, r)?;
        let course = Course::from_reader(self.width, self.length, r)?;
        Ok(RaceStep {
            race: self,
            current_step,
            rest_planning_time,
            players: [p_self, p_other],
            course,
        })
    }
}

impl<'a> RaceStep<'a> {
    /// [`SimulatedRaceStep`]を生成する
    ///
    /// [`Course`]を独自に生成しないが、ゲーム木の探索はしたい場合に使う便利メソッド。
    ///
    /// [`SimulatedRaceStep`]: struct.SimulatedRaceStep.html
    /// [`Course`]: struct.Course.html
    pub fn to_simulated(&'a self) -> SimulatedRaceStep<'a> {
        SimulatedRaceStep {
            race: self.race,
            current_step: self.current_step,
            players: [self.players[0].clone(), self.players[1].clone()],
            course: &self.course,
        }
    }
}

impl<'a> SimulatedRaceStep<'a> {
    /// [`Course`]を独自に生成した場合に使う生成関数
    ///
    /// [`Course`]: struct.Course.html
    pub fn new(race_step: &'a RaceStep, course: &'a Course) -> SimulatedRaceStep<'a> {
        SimulatedRaceStep {
            race: race_step.race,
            current_step: race_step.current_step,
            players: [race_step.players[0].clone(), race_step.players[1].clone()],
            course,
        }
    }

    /// 両者の加速度を入力にしてゲームをシミュレートし、次のステップでの状態を返す
    pub fn simulate(&self, accelerations: [Vec2; 2]) -> SimulatedRaceStep<'a> {
        for a in &accelerations {
            debug_assert!(a.x == 0 || a.x == 1 || a.x == -1);
            debug_assert!(a.y == 0 || a.y == 1 || a.y == -1);
        }
        let mut move_a = {
            let movement = self.players[0].get_movement(accelerations[0]);
            if self.players[0].is_racing() && self.course.can_move(&movement) {
                movement
            } else {
                Movement::new(movement.from, movement.from)
            }
        };
        let mut move_b = {
            let movement = self.players[1].get_movement(accelerations[1]);
            if self.players[1].is_racing() && self.course.can_move(&movement) {
                movement
            } else {
                Movement::new(movement.from, movement.from)
            }
        };
        if self.players[0].is_racing() && self.players[1].is_racing() {
            // 両者がレース中かつ衝突した場合
            if move_a.intersects(&move_b) {
                let a_has_priority = !move_a.touched_pos().any(|v| v == move_b.from);
                let b_has_priority = !move_b.touched_pos().any(|v| v == move_a.from);
                if a_has_priority && b_has_priority {
                    // 両者とも優先権を持っていた場合
                    // 1. yが小さい方
                    // 2. xが小さい方
                    // の順で優先権を獲得する
                    if move_a.from.y < move_b.from.y
                        || (move_a.from.y == move_b.from.y && move_a.from.x < move_b.from.x)
                    {
                        move_b = Movement::new(move_b.from, move_b.from);
                    } else {
                        move_a = Movement::new(move_a.from, move_a.from);
                    }
                } else if a_has_priority {
                    move_b = Movement::new(move_b.from, move_b.from);
                } else if b_has_priority {
                    move_a = Movement::new(move_a.from, move_a.from);
                } else {
                    move_a = Movement::new(move_a.from, move_a.from);
                    move_b = Movement::new(move_b.from, move_b.from);
                }
            }
        }
        let player_a = {
            let velocity = if self.course.ikepocha(move_a.to) {
                Vec2::new(0, 0)
            } else {
                move_a.to - move_a.from
            };
            Player::new(self.race.length, move_a.to, velocity)
        };
        let player_b = {
            let velocity = if self.course.ikepocha(move_b.to) {
                Vec2::new(0, 0)
            } else {
                move_b.to - move_b.from
            };
            Player::new(self.race.length, move_b.to, velocity)
        };
        SimulatedRaceStep {
            race: self.race,
            current_step: self.current_step + 1,
            players: [player_a, player_b],
            course: &self.course,
        }
    }

    /// 自分のプレイヤがレース中か判定する
    ///
    /// ショートカット的意味が強い
    pub fn self_racing(&self) -> bool {
        self.players[0].is_racing()
    }

    /// 探索に置いて両プレイヤの位置と速度が完全に同じならそれは同じ状態だと思って良い
    pub fn play_key(&self) -> (Vec2, Vec2, Vec2, Vec2) {
        (
            self.players[0].position,
            self.players[0].velocity,
            self.players[1].position,
            self.players[1].velocity,
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::BufReader;

    #[test]
    fn race_from_reader() {
        let mut input = BufReader::new(
            r#"120000
100
16 20
10
3
118628
5 1 0 0
4 6 -1 3
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0 1 0 1 1 1
2 2 2 2 2 0 2 2 2 0 0 0 0 0 0 0
2 2 2 2 2 2 2 1 1 1 1 0 0 0 0 0
2 2 2 2 0 0 2 2 0 0 0 0 0 0 0 0
2 2 2 2 0 0 0 2 0 0 0 0 0 0 0 0
2 2 2 2 2 2 2 2 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 1 1 1 1 1 1 1 0 0 0 0 0
0 0 0 0 0 1 1 1 1 1 1 0 0 0 0 0
0 0 0 0 0 1 0 1 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
0 2 2 2 0 0 0 0 0 0 0 1 1 1 0 0
0 2 2 2 0 0 0 1 1 1 1 1 1 1 0 0
0 0 0 0 0 0 2 2 2 1 1 1 0 0 0 0
-1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1
-1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1
-1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1
-1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1
"#
            .as_bytes(),
        );
        let r = Race::from_reader(&mut input).unwrap();
        assert_eq!(r.planning_time_limit, 120000);
        assert_eq!(r.step_limit, 100);
        assert_eq!(r.width, 16);
        assert_eq!(r.length, 20);
        assert_eq!(r.vision, 10);
        let step = r.step_from_reader(&mut input).unwrap();
        assert_eq!(step.current_step, 3);
        assert_eq!(step.rest_planning_time, 118628);
        assert!(step.players[0].is_racing());
        assert!(step.players[1].is_racing());
    }

    #[test]
    fn simulate_1() {
        let mut input = BufReader::new(
            r#"120000
100
10 9
10
3
118628
5 0 0 0
4 6 0 2
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0
2 2 2 2 2 0 2 2 2 0
2 2 2 2 2 2 2 1 1 1
2 2 2 2 0 0 2 2 0 0
2 2 2 2 0 0 0 2 0 0
2 2 2 2 2 2 2 2 0 0
0 0 0 0 0 1 1 1 1 1
"#
            .as_bytes(),
        );
        let r = Race::from_reader(&mut input).unwrap();
        let step = r.step_from_reader(&mut input).unwrap();
        let sim = step.to_simulated();
        let test_cases = &[
            (
                [vec2!(0, 0), vec2!(0, 0)], // `simulate`用の加速度のペア
                [vec2!(5, 0), vec2!(0, 0)], // player0期待される位置と速度
                [vec2!(4, 8), vec2!(0, 2)], // player1期待される位置と速度
            ),
            (
                [vec2!(1, 0), vec2!(1, 0)],
                [vec2!(6, 0), vec2!(1, 0)],
                [vec2!(4, 6), vec2!(0, 0)],
            ),
            (
                [vec2!(-1, 0), vec2!(-1, -1)],
                [vec2!(4, 0), vec2!(-1, 0)],
                [vec2!(3, 7), vec2!(0, 0)],
            ),
            (
                [vec2!(0, -1), vec2!(0, 1)],
                [vec2!(5, -1), vec2!(0, -1)],
                [vec2!(4, 9), vec2!(0, 3)],
            ),
        ];
        for case in test_cases {
            let nex = sim.simulate(case.0);
            let pa = Player::new(r.length, case.1[0], case.1[1]);
            let pb = Player::new(r.length, case.2[0], case.2[1]);
            assert_eq!(nex.players, [pa, pb]);
        }
    }

    #[test]
    fn simulate_2() {
        let mut input = BufReader::new(
            r#"120000
100
5 3
10
3
118628
1 0 0 0
2 0 0 0
0 0 0 0 0
0 0 0 0 0
0 0 0 0 0
"#
            .as_bytes(),
        );
        let r = Race::from_reader(&mut input).unwrap();
        let step = r.step_from_reader(&mut input).unwrap();
        let sim = step.to_simulated();
        let test_cases = &[
            // 衝突しない
            (
                [vec2!(0, 1), vec2!(0, 1)], // `simulate`用の加速度のペア
                [vec2!(1, 1), vec2!(0, 1)], // player0期待される位置と速度
                [vec2!(2, 1), vec2!(0, 1)], // player1期待される位置と速度
            ),
            // 衝突するときxが小さい方に優先権がある
            (
                [vec2!(0, 1), vec2!(-1, 1)],
                [vec2!(1, 1), vec2!(0, 1)],
                [vec2!(2, 0), vec2!(0, 0)],
            ),
            // お互いの所に行こうとすると両方とも動かない
            (
                [vec2!(1, 0), vec2!(-1, 0)],
                [vec2!(1, 0), vec2!(0, 0)],
                [vec2!(2, 0), vec2!(0, 0)],
            ),
            // スタート地点の判定が最初
            (
                [vec2!(1, 0), vec2!(0, 1)],
                [vec2!(1, 0), vec2!(0, 0)],
                [vec2!(2, 1), vec2!(0, 1)],
            ),
        ];
        for case in test_cases {
            let nex = sim.simulate(case.0);
            let pa = Player::new(r.length, case.1[0], case.1[1]);
            let pb = Player::new(r.length, case.2[0], case.2[1]);
            assert_eq!(nex.players, [pa, pb]);
        }
    }

    #[test]
    fn simulate_3() {
        let mut input = BufReader::new(
            r#"120000
100
3 5
10
3
118628
1 1 1 3
2 0 -1 4
0 0 0
0 0 0
0 0 0
0 0 0
0 0 0
"#
            .as_bytes(),
        );
        let r = Race::from_reader(&mut input).unwrap();
        let step = r.step_from_reader(&mut input).unwrap();
        let sim = step.to_simulated();
        let test_cases = &[
            // yが小さい(xは大きい)方が優先権をもつ
            (
                [vec2!(0, 0), vec2!(0, 0)],  // `simulate`用の加速度のペア
                [vec2!(1, 1), vec2!(0, 0)],  // player0期待される位置と速度
                [vec2!(1, 4), vec2!(-1, 4)], // player1期待される位置と速度
            ),
            // 衝突による優先権の交代
            (
                [vec2!(0, 0), vec2!(0, -1)],
                [vec2!(2, 4), vec2!(1, 3)],
                [vec2!(2, 0), vec2!(0, 0)],
            ),
        ];
        for case in test_cases {
            let nex = sim.simulate(case.0);
            let pa = Player::new(r.length, case.1[0], case.1[1]);
            let pb = Player::new(r.length, case.2[0], case.2[1]);
            assert_eq!(nex.players, [pa, pb]);
        }
    }
}
